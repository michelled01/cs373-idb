ifeq ($(OS),Windows_NT)          # Windows
	MOVE := move "backend\/api.schema.html" "models.html"
else                             # *nix
	MOVE := mv backend/api.schema.html models.html
endif

IDB3.log:
	git log > IDB3.log

models.html: backend/api/schema.py
	cd backend && python -m pydoc -w api.schema
	$(MOVE)

test:
	cd backend && python tests.py
