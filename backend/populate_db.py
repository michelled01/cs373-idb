from xml2dict import XmlDictConfig
import json
import os
from glob import glob
from xml.etree import cElementTree as ElementTree
from api.schema import db, Bill, Congressperson, Committee, Action
from api import app
from itertools import count

DATA_PATH = os.path.dirname(__file__) + "/scrape/data/"
FILENAMES = [
    "congresspeople.json",
    "committees.json",
]
BULK_DATA_PATH = os.path.dirname(__file__) + "/scrape/bulk/"
BULK_FILENAMES = [
    "BILLSTATUS-118-hconres",
    "BILLSTATUS-118-hjres",
    "BILLSTATUS-118-hres",
    "BILLSTATUS-118-hr",
    "BILLSTATUS-118-sconres",
    "BILLSTATUS-118-sjres",
    "BILLSTATUS-118-sres",
    "BILLSUM-118-hconres",
    "BILLSUM-118-hjres",
    "BILLSUM-118-hres",
    "BILLSUM-118-hr",
    "BILLSUM-118-sconres",
    "BILLSUM-118-sjres",
    "BILLSUM-118-sres",
    "BILLSUM-118-s",
    "committee-membership-current.json",
    "committees-current.json",
    "legislators-current.json",
    "votes",
]

STATES = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "FL": "Florida",
    "GA": "Georgia",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "DC": "Columbia",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PA": "Pennsylvania",
    "PR": "Puerto Rico",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "MT": "Montana",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming",
    "DC": "District of Columbia",
    "MP": "Northern Mariana Islands",
    "AS": "American Samoa",
    "GU": "Guam",
    "PR": "Puerto Rico",
    "VI": "Virgin Islands",
    "TT": "Trust Territories",
}

class Progress(object):
    def __init__(self, total):
        self.total = total
        self.so_far = 0

    def __next__(self):
        if self.so_far > self.total:
            raise StopIteration
        self.so_far += 1
        return f"{self.so_far: >8}/{self.total} [{self.so_far / self.total:.2%}]"

# map between sequential positive integer IDs (what we want for our DB)
# and some other sort of ID/object (what the source data contains)
class Index(object):
    def __init__(self):
        self.counter = count(1)
        self.object_to_id = {}
        self.id_to_object = {}

    def __getitem__(self, x):
        if type(x) == int:
            if x not in self.id_to_object.keys():
                return None
            return self.id_to_object[x]
        else:
            if x not in self.object_to_id.keys():
                c = next(self.counter)
                self.object_to_id[x] = c
                self.id_to_object[c] = x
            return self.object_to_id[x]
        
    def __len__(self):
        return len(self.object_to_id)

# primary keys for the 3 models
bill_ids = count(1)
congressperson_ids = Index() # bioguide ID -> internal ID
committee_ids = Index() # ID without the 00 at the end -> internal ID

# load in data from scraper scripts
# map internal ID -> item
scraped_committees = {}
for committee in json.load(open(f"{DATA_PATH}committees.json")):
    id = committee_ids[committee["committee"]["systemCode"][:4].upper()]
    scraped_committees[id] = committee["committee"]
    scraped_committees[id]["chamber"] = committee["request"]["chamber"][0].upper() + committee["request"]["chamber"][1:]

scraped_congresspeople = {
    congressperson_ids[member["member"]["bioguideId"]]: member["member"]
    for member in json.load(open(f"{DATA_PATH}congresspeople.json"))
}

# load in bulk data
congresspeople = json.load(open(f"{BULK_DATA_PATH}legislators-current.json"))
committees = json.load(open(f"{BULK_DATA_PATH}committees-current.json"))
committee_membership = json.load(open(f"{BULK_DATA_PATH}committee-membership-current.json"))
votes = {}
for filename in glob(f"{BULK_DATA_PATH}votes/*/data.json"):
    vote = json.load(open(filename))
    if "bill" in vote.keys():
        votes[(
            vote["bill"]["congress"],
            vote["bill"]["type"].upper(),
            vote["bill"]["number"],
        )] = vote

# source data has IDs represented in multiple formats
bioguide_to_lis = {}
lis_to_bioguide = {}
for congressperson in congresspeople:
    try:
        bioguide_to_lis[congressperson["id"]["bioguide"]] = congressperson["id"]["lis"]
        lis_to_bioguide[congressperson["id"]["lis"]] = congressperson["id"]["bioguide"]
    except Exception as e:
        bioguide_to_lis[congressperson["id"]["bioguide"]] = None
    
# source data often has several items of the same kind with different dates
# we only care about the most recent
def get_most_recent(items, seeking, key, zero="0000-00-00"):
    most_recent_item = None
    most_recent_date = zero
    for item in items:
        if most_recent_date < key(item):
            most_recent_date = key(item)
            most_recent_item = seeking(item)
    return most_recent_item

# xml2dict doesn't put single values in a list, which breaks uniformity of the dict tree structure
def ensure_list(item):
    if not isinstance(item, list):
        item = [item]
    return item

# all files must be present to continue
# does not include roll calls because these arent available as bulk downloads (these are fetched below as needed)
def bulk_data_dir_is_ok():
    ok = True
    for filename in FILENAMES:
        if not os.path.exists(f"{DATA_PATH}{filename}"):
            print(f"Missing file {filename}! Aborting.")
            ok = False
    for filename in BULK_FILENAMES:
        if not os.path.exists(f"{BULK_DATA_PATH}{filename}"):
            print(f"Missing file {filename}! Aborting.")
            ok = False
    return ok

# map internal ID (e.g. 1, 2, 1729) -> row object (i.e. Bill, Congressperson, Committee)
# used to track existing model instances
class Tracker(object):
    def __init__(self, model):
        self.model = model
        self.tracker = {}

    def __getitem__(self, id):
        if self.model == Committee:
            assert id < 54
        assert type(id) == int
        if id not in self.tracker.keys():
            self.tracker[id] = self.model(id=id)
        return self.tracker[id]
    
    def __len__(self):
        return len(self.tracker)

bill_tracker = Tracker(model=Bill)
congressperson_tracker = Tracker(model=Congressperson)
committee_tracker = Tracker(model=Committee)

def populate_db():
    populate_bills()
    populate_congresspeople()
    populate_committees()

    populate_bill_relations()
    populate_congressperson_relations()
    populate_committee_relations()

def populate_bills():
    billstatus_filenames = glob(f"{BULK_DATA_PATH}BILLSTATUS*/*.xml")
    progress = Progress(len(billstatus_filenames))
    for billstatus_filename in billstatus_filenames:
        print(f"bills: {next(progress)}")
        billstatus = XmlDictConfig(ElementTree.parse(billstatus_filename).getroot())

        # flesh out existing dummy Bill if one exists, otherwise make a new one
        id = next(bill_ids)
        bill = bill_tracker[id]

        bill.id = id
        bill.type = billstatus["bill"]["type"]
        bill.number = billstatus["bill"]["number"]
        bill.title = billstatus["bill"]["title"]
        if "summaries" in billstatus["bill"]:
            bill.description = get_most_recent(ensure_list(billstatus["bill"]["summaries"]["summary"]), lambda summary : summary["text"], lambda summary : summary["actionDate"])
        else:
            bill.description = None
        bill.congress = billstatus["bill"]["congress"]
        bill.chamber = billstatus["bill"]["originChamber"]
        if "policyArea" in billstatus["bill"].keys():
            bill.subject_area = billstatus["bill"]["policyArea"]["name"]
        else:
            bill.subject_area = None
        
        # get most recent
        bill.full_text_link = None
        most_recent_text_date = "0000-00-00"
        if "textVersions" in billstatus["bill"].keys():
            for text in ensure_list(billstatus["bill"]["textVersions"]["item"]):
                try:
                    if most_recent_text_date < text["date"][:10]:
                        most_recent_text_date = text["date"][:10]
                        bill.full_text_link = ensure_list(text["formats"]["item"])[0]["url"]
                except:
                    pass

        # actions
        debug_duplicate_actions = False
        
        if debug_duplicate_actions == True:
            print("ATTACH ACTIONS TO BILLS")
            print(f"bill.id = {bill.id}")

        sequence_number = 0

        for a in ensure_list(billstatus["bill"]["actions"]["item"]):

            append = True
            actionDate = a['actionDate']
            text = a['text']
            
            if len(bill.actions) > 0:
                prev = bill.actions[len(bill.actions) - 1]
 
                if prev.date == actionDate and prev.action == text:
                    if debug_duplicate_actions == True:
                        print("DUPLICATE ACTION")
                        print(f"prev.sequence_number = {prev.sequence_number}")
                        print(f"prev.date = {prev.date}, a[\"actionDate\"] = {actionDate}")
                        print(f"prev.action = {prev.action}, a[\"text\"] = {text}")
                    append = False

            if append == True:
                if debug_duplicate_actions == True:
                    print(f"APPEND ACTION")
                    print(f"{sequence_number}: date = {actionDate}, text = {text}")
                action = Action()
                action.bill_id = bill.id
                action.bill = bill
                action.sequence_number = sequence_number
                action.date = actionDate
                action.action = text
                bill.actions.append(action)
                sequence_number += 1
            
        db.session.add(bill)
    db.session.commit()

def populate_bill_relations():
    progress = Progress(len(bill_tracker.tracker.values()))
    for bill in bill_tracker.tracker.values():
        print(f"bill relations: {next(progress)}")

        billstatus_filename = f"{BULK_DATA_PATH}BILLSTATUS-{bill.congress}-{bill.type.lower()}/BILLSTATUS-{bill.congress}{bill.type.lower()}{bill.number}.xml"
        billstatus = XmlDictConfig(ElementTree.parse(billstatus_filename).getroot())

        # votes yea and nay
        id3 = (bill.congress, bill.type, bill.number)
        if id3 in votes.keys():
            vote = votes[id3]
            for voted_yn, yn in ((bill.voted_yea, "Yea"), (bill.voted_yea, "Aye"), (bill.voted_nay, "Nay"), (bill.voted_nay, "No")):
                try:
                    for member in vote["votes"][yn]:
                        mid = member["id"]
                        if len(mid) == 4:
                            mid = lis_to_bioguide[mid]
                        congressperson = congressperson_tracker[congressperson_ids[mid]]
                        voted_yn.append(congressperson)
                except:
                    pass

        # sponsors
        for sponsor in ensure_list(billstatus["bill"]["sponsors"]):
            sponsor_id = congressperson_ids[sponsor["item"]["bioguideId"]]
            congressperson_row = congressperson_tracker[sponsor_id]

            bill.sponsors.append(congressperson_row)

        # committees
        if "committees" in billstatus["bill"].keys():
            for committee in ensure_list(billstatus["bill"]["committees"]["item"]):
                committee_id = committee_ids[committee["systemCode"][:4].upper()]
                if committee_id not in scraped_committees.keys():
                    continue
                committee_row = committee_tracker[committee_id]

                bill.committees.append(committee_row)
        else:
            bill.committees = []

        db.session.add(bill)
    db.session.commit()

def populate_congresspeople():
    progress = Progress(len(congresspeople))
    for person in congresspeople:
        print(f"congresspeople: {next(progress)}")
        # flesh out existing dummy Congressperson if one exists, otherwise make a new one
        id = congressperson_ids[person["id"]["bioguide"]]
        congressperson = congressperson_tracker[id]
        scraped_congressperson = scraped_congresspeople[id]

        congressperson.id = id
        congressperson.first_name = person["name"]["first"]
        congressperson.last_name = person["name"]["last"]
        if "middleName" in scraped_congressperson.keys():
            congressperson.middle_initial = scraped_congressperson["middleName"][0]
        else:
            congressperson.middle_initial = None

        most_recent_term = get_most_recent(person["terms"], lambda term : term, lambda term : term["start"])

        congressperson.chamber = "House" if most_recent_term["type"] == "rep" else "Senate"
        congressperson.party = most_recent_term["party"]
        congressperson.party_initial = most_recent_term["party"][0]
        congressperson.district = most_recent_term["district"] if "district" in most_recent_term.keys() else None
        congressperson.state = STATES[most_recent_term["state"]]
        congressperson.state_abbreviation = most_recent_term["state"]
        if "depiction" in scraped_congressperson.keys():
            congressperson.image_url = scraped_congressperson["depiction"]["imageUrl"]
        else:
            congressperson.image_url = None
        if "url" in most_recent_term.keys():
            congressperson.website_url = most_recent_term["url"]
        else:
            congressperson.website_url = None

        db.session.add(congressperson)
    db.session.commit()

def populate_congressperson_relations():
    progress = Progress(len(congressperson_tracker.tracker.values()))
    with db.session.no_autoflush:
        for congressperson in congressperson_tracker.tracker.values():
            print(f"congressperson relations: {next(progress)}")

            # committee membership and chairing status
            congressperson.chairing = []
            for committee_id, committee in committee_membership.items():
                if len(committee_id) == 4 and committee_ids[committee_id] in scraped_committees.keys(): # is a committee rather than a subcommittee
                    for member in ensure_list(committee):
                        if congressperson_ids[member["bioguide"]] == congressperson.id: #person["id"]["bioguide"]:
                            cid = committee_ids[committee_id]
                            committee_tracker[cid].congressperson_id = congressperson.id
                            committee_row = committee_tracker[cid]

                            assert committee_row.congressperson_id != None
                            assert congressperson.id != None
                            assert type(committee_row.congressperson_id) == int
                            assert type(congressperson.id) == int

                            congressperson.committees.append(committee_row)
                            if "rank" in member.keys() and member["rank"] == 1:
                                congressperson.chairing.append(committee_row)

            # bill relations
            congressperson.sponsored_bills = db.session.query(Bill).where(Bill.sponsors.contains(congressperson)).all()
            congressperson.bills_voted_yea = db.session.query(Bill).where(Bill.voted_yea.contains(congressperson)).all()
            congressperson.bills_voted_nay = db.session.query(Bill).where(Bill.voted_nay.contains(congressperson)).all()

            db.session.add(congressperson)
    db.session.commit()

def populate_committees():
    progress = Progress(len(committees))
    for com in committees:
        print(f"committees: {next(progress)}")
        # flesh out existing dummy Committee if one exists, otherwise make a new one
        id = committee_ids[com["thomas_id"]]
        try:
            scraped_committee = scraped_committees[id]
        except:
            print(id, committee_ids[id])
            continue # if it doesn't exist on api.congress.gov, it doesn't exist
        committee = committee_tracker[id]

        committee.id = id
        committee.name = com["name"]
        committee.type = scraped_committee["type"]
        committee.chamber = scraped_committee["chamber"]
        committee.active = scraped_committee["isCurrent"]
        committee.website_url = com["url"] if "url" in com.keys() else None

        # find earliest date if such exists
        committee.established_date = "9999-99-99"
        for version in scraped_committee["history"]:
            if committee.established_date > version["startDate"][:10]:
                committee.established_date = version["startDate"][:10]
        if committee.established_date == "9999-99-99":
            committee.established_date = None

        # chair ID
        chair_id = None
        for committee_id, cm in committee_membership.items():
            if len(committee_id) == 4 and committee_ids[committee_id] in scraped_committees.keys(): # is a committee rather than a subcommittee
                cid = committee_ids[committee_id]
                if cid == id:
                    for member in ensure_list(cm):
                        if "rank" in member.keys() and member["rank"] == 1:
                            chair_id = congressperson_ids[member["bioguide"]]
                            break
                    else:
                        print("No chair among members!")
                    break
        else:
            print(f"Couldn't find the chair ID for committee {committee.id} {committee.name}!")
        assert chair_id != None
        committee.congressperson_id = chair_id

        db.session.add(committee)
    db.session.commit()

def populate_committee_relations():
    progress = Progress(len(committee_tracker.tracker.values()))
    for committee in committee_tracker.tracker.values():
        print(f"committee relations: {next(progress)}")
        if committee.id not in scraped_committees.keys():
            continue

        # chair ID
        chair_id = None
        for committee_id, cm in committee_membership.items():
            if len(committee_id) == 4 and committee_ids[committee_id] in scraped_committees.keys(): # is a committee rather than a subcommittee
                cid = committee_ids[committee_id]
                if cid == committee.id:
                    for member in ensure_list(cm):
                        if "rank" in member.keys() and member["rank"] == 1:
                            chair_id = congressperson_ids[member["bioguide"]]
                            break
                    else:
                        print("No chair among members!")
                    break
        else:
            print(f"Couldn't find the chair ID for committee {committee.id} {committee.name}!")
        assert chair_id != None
        chair = congressperson_tracker[chair_id]
        committee.chair = chair

        committee.members = db.session.query(Congressperson).where(Congressperson.committees.contains(committee)).all()
        committee.bills = db.session.query(Bill).where(Bill.committees.contains(committee)).all()

        db.session.add(committee)
    db.session.commit()

# to be manually looked over
def sanity_check():
    print(f"len(congressperson_ids) = {len(congressperson_ids)}")
    print(f"len(committee_ids) = {len(committee_ids)}")
    # print(committee_ids.object_to_id)
    print(f"len(bioguide_to_lis) = {len(bioguide_to_lis)}")
    # print(bioguide_to_lis)
    print(f"len(lis_to_bioguide) = {len(lis_to_bioguide)}")
    # print(lis_to_bioguide)

if __name__ == "__main__":
    if bulk_data_dir_is_ok():
        app.app_context().push()

        db.drop_all()
        db.create_all()
        populate_db()
        db.session.commit()
        sanity_check()
