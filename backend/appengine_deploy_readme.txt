# deploy_readment.txt

OVERVIEW

   Instructions for deploying the back end services to google cloud.
   If you're reading this you, are already in the backend directory.
   If not, then first, in a terminal, navigate to the backend directory.


SETUP

1. In the backend directory, create a .env file.
   
   Add the required string(s):

      DB_CONNECTION_STRING='${APPENGINE_DB_CONNECTION_STRING}'

   You'll need to retrieve the actual connection string(s) from gitlab ci/cd keys at:

      https://gitlab.com/michelled01/cs373-idb/-/settings/ci_cd

   The key is: 

      APPENGINE_DB_CONNECTION_STRING_ENCODED

   In a temp directory someplace, create a text file, e.g. ENCODED_SECRETS.txt.
   
   Copy the encoded value into it and save.

   Then in a terminal:

      cat ENCODED_SECRETS.txt | base64 -d

   The unencoded key is:

      APPENGINE_DB_CONNECTION_STRING

   The encoded secret was created by placing what you see at the decoded values into
   a text file, e.g. SECRETS.txt, and running the following command:

      cat SECRETS.txt | base64 -w0 | tee ENCODED_SECRETS.txt


2. DO NOT COMMIT the .env to git
   
   If updates need to be made, add it to the previous list of required tokens.


DEPLOYMENT

1. Get the latest sha for HEAD in whatever branch you are in

   git rev-parse --short HEAD

   e.g.

   "084360c"


2. Add the sha to a string describing the purpose of the deployment

   e.g.

   "phase2-db-enabled"


3. Now you have a string that looks like this:

   "084360c-phase2-db-enabled"


4. Deploy without a promote:

   gcloud app deploy --version="084360c-phase2-db-enabled" --no-promote


5. You'll be asked to confirm the deploy.

   $gcloud app deploy --version="084360c-phase2-db-enabled" --no-promote
   Services to deploy:

   descriptor:                  [/home/bean/work/cs373-idb/backend/app.yaml]
   source:                      [/home/bean/work/cs373-idb/backend]
   target project:              [legislore]
   target service:              [default]
   target version:              [084360c-phase2-db-enabled]
   target url:                  [https://084360c-phase2-db-enabled-dot-legislore.uc.r.appspot.com]
   target service account:      [gitlab-deploy@legislore.iam.gserviceaccount.com]


       (add --promote if you also want to make this service available from
       [https://legislore.uc.r.appspot.com])

   Do you want to continue (Y/n)?  Y


6. Expected output:

   Beginning deployment of service [default]...
   ╔════════════════════════════════════════════════════════════╗
   ╠═ Uploading 0 files to Google Cloud Storage                ═╣
   ╚════════════════════════════════════════════════════════════╝
   File upload done.
   Updating service
   [default]...done.                                                                                                                                                                                         
   Deployed service [default] to [https://084360c-phase2-db-enabled-dot-legislore.uc.r.appspot.com]

   You can stream logs from the command line by running:
   $ gcloud app logs tail -s default

   To view your application in the web browser run:
   $ gcloud app browse


7. To deploy a service you can navigate to it in the cloud UI and select to promote it.
   Otherwise, just rerun the deply but leave off the trailing --no-deploy.
   Then it will change https://legislore.uc.r.appspot.com as well as deploying
   to the specified "version".


HOUSEKEEPING

   Take the time to see how many services are deployed in the cloud ui. Delete those
   that are unnecessary.

