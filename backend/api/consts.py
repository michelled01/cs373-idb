import re
from operator import eq, ne, lt, gt, le, ge
from .schema import Bill, Congressperson, Committee, votes_yea, votes_nay, sponsorings, members, actions_by_committee
from sqlalchemy import func, distinct

OPERATORS = {
    "eq": eq,
    "ne": ne,
    "lt": lt,
    "gt": gt,
    "le": le,
    "ge": ge,
}

DEFAULTS = {
    "page": 1,
    "items_per_page": 10,
    "sort_by": None,
    "sort_ascending": True,
    "search": "",
    "search_conjunction": "and",
}

TYPES = {
    "page": int,
    "items_per_page": int,
    "sort_by": str,
    "sort_ascending": lambda b : False if b.lower() == "false" else True,
    "search": str,
    "search_conjunction": str,
}

TABLES = {
    "bill": Bill,
    "congressperson": Congressperson,
    "committee": Committee,
}

SORTABLE_FIELDS = {
    "bill": ["id", "name.type", "name.number", "name.title", "congress", "chamber", "subject_area", "total_committees", "total_sponsors", "total_votes_yea", "total_votes_nay"],
    "congressperson": ["id", "name.first", "name.last", "name.middle_initial", "total_committees", "total_sponsored_bills", "total_votes_yea", "total_votes_nay", "party", "party_initial", "chamber", "district", "state", "state_abbreviation"],
    "committee": ["id", "name", "type", "chamber", "established_date", "active", "total_members", "total_bills"],
}

EQUALITY_COMPARABLE_FIELDS = SORTABLE_FIELDS

INEQUALITY_COMPARABLE_FIELDS = {
    "bill": ["id", "name.number", "congress", "total_committees", "total_sponsors", "total_votes_yea", "total_votes_nay"],
    "congressperson": ["id", "total_committees", "total_sponsored_bills", "total_votes_yea", "total_votes_nay", "district"],
    "committee": ["id", "total_members", "total_bills"],
}

DATE_FIELDS = {
    "bill": [],
    "congressperson": [],
    "committee": ["established_date"],
}

ALL_FIELDS = {
    "bill": {
        "id": Bill.id,
        "name.type": Bill.type,
        "name.number": Bill.number,
        "name.title": Bill.title,
        "description": Bill.description,
        "congress": Bill.congress,
        "chamber": Bill.chamber,
        "subject_area": Bill.subject_area,
        "actions": Bill.actions,
        "committees": Bill.committees,
        "sponsors": Bill.sponsors,
        "voted_yea": Bill.voted_yea,
        "voted_nay": Bill.voted_nay,
        "total_committees": (actions_by_committee.c.committee_id, actions_by_committee),
        "total_sponsors": (sponsorings.c.congressperson_id, sponsorings),
        "total_votes_yea": (votes_yea.c.congressperson_id, votes_yea),
        "total_votes_nay": (votes_nay.c.congressperson_id, votes_nay),
    },
    "congressperson": {
        "id": Congressperson.id,
        "name.last": Congressperson.last_name,
        "name.first": Congressperson.first_name,
        "name.middle_initial": Congressperson.middle_initial,
        "chamber": Congressperson.chamber,
        "party": Congressperson.party,
        "party_initial": Congressperson.party_initial,
        "district": Congressperson.district,
        "state": Congressperson.state,
        "state_abbreviation": Congressperson.state_abbreviation,
        "image_url": Congressperson.image_url,
        "website_url": Congressperson.website_url,
        "committees": Congressperson.committees,
        "sponsored_bills": Congressperson.sponsored_bills,
        "bills_voted_yea": Congressperson.bills_voted_yea,
        "bills_voted_nay": Congressperson.bills_voted_nay,
        "total_committees": (members.c.committee_id, members),
        "total_sponsored_bills": (sponsorings.c.bill_id, sponsorings),
        "total_votes_yea": (votes_yea.c.bill_id, votes_yea),
        "total_votes_nay": (votes_nay.c.bill_id, votes_nay),
    },
    "committee": {
        "id": Committee.id,
        "name": Committee.name,
        "type": Committee.type,
        "chamber": Committee.chamber,
        "established_date": Committee.established_date,
        "active": Committee.active,
        "website_url": Committee.website_url,
        "chair": Committee.chair,
        "members": Committee.members,
        "bills": Committee.bills,
        "total_members": (members.c.congressperson_id, members),
        "total_bills": (actions_by_committee.c.bill_id, actions_by_committee),
    }
}

DISPLAY_NAMES = {
    "bill": {
        "id": "ID",
        "name.type": "Type",
        "name.number": "Number",
        "name.title": "Title",
        "description": "Description",
        "congress": "Congress Number",
        "chamber": "Chamber of Origin",
        "subject_area": "Subject Area",
        "actions": "Actions on Bill",
        "committees": "Committees Having Seen Bill",
        "sponsors": "Sponsors",
        "voted_yea": "Congresspeople who Voted Yea",
        "voted_nay": "Congresspeople who Voted Nay",
        "total_committees": "Total Committees Having Seen Bill",
        "total_sponsors": "Total Sponsors",
        "total_votes_yea": "Total Votes Yea on Bill",
        "total_votes_nay": "Total Votes Nay on Bill",
    },
    "congressperson": {
        "id": "ID",
        "name.first": "First Name",
        "name.last": "Last Name",
        "name.middle_initial": "Middle Initial",
        "chamber": "Chamber",
        "party": "Party",
        "party_initial": "Party Initial",
        "district": "District (for Representatives)",
        "state": "State",
        "state_abbreviation": "State Abbreviation",
        "image_url": "Image URL",
        "website_url": "Website URL",
        "committees": "Committees in",
        "sponsored_bills": "Sponsored Bills",
        "bills_voted_yea": "Bills Voted Yea on",
        "bills_voted_nay": "Bills Voted Nay on",
        "total_committees": "Total Committees in",
        "total_sponsored_bills": "Total Sponsored Bills",
        "total_votes_yea": "Total Bills Voted Yea on",
        "total_votes_nay": "Total Bills Voted Nay on",
    },
    "committee": {
        "id": "ID",
        "name": "Name",
        "type": "Type",
        "chamber": "Chamber",
        "established_date": "Date Established",
        "active": "Currently Active",
        "website_url": "Website URL",
        "chair": "Chair",
        "members": "Members",
        "bills": "Bills Seen",
        "total_members": "Total Members",
        "total_bills": "Total Bills Seen",
    }
}

FIELD_TYPES = {
    "bill": {
        "id": "number",
        "name.type": "string",
        "name.number": "number",
        "name.title": "string",
        "description": "string",
        "congress": "number",
        "chamber": "string",
        "subject_area": "string",
        "actions": "",
        "committees": "",
        "sponsors": "",
        "voted_yea": "",
        "voted_nay": "",
        "total_committees": "number",
        "total_sponsors": "number",
        "total_votes_yea": "number",
        "total_votes_nay": "number",
    },
    "congressperson": {
        "id": "number",
        "name.last": "string",
        "name.first": "string",
        "name.middle_initial": "string",
        "chamber": "string",
        "party": "string",
        "party_initial": "string",
        "district": "number",
        "state": "string",
        "state_abbreviation": "string",
        "image_url": "string",
        "website_url": "string",
        "committees": "",
        "sponsored_bills": "",
        "bills_voted_yea": "",
        "bills_voted_nay": "",
        "total_committees": "number",
        "total_sponsored_bills": "number",
        "total_votes_yea": "number",
        "total_votes_nay": "number",
    },
    "committee": {
        "id": "number",
        "name": "string",
        "type": "string",
        "chamber": "string",
        "established_date": "date (YYYY-MM-DD)",
        "active": "bool ('true' or 'false')",
        "website_url": "string",
        "chair": "",
        "members": "",
        "bills": "",
        "total_members": "number",
        "total_bills": "number",
    }
}

INEQUALITY_COMPARABLE_FIELD_PATTERN = re.compile(r"(eq|ne|[gl][et])_[0-9]+")
EQUALITY_COMPARABLE_FIELD_PATTERN = re.compile(r"(eq|ne)_.+")
DATE_FIELD_PATTERN = re.compile(r"(eq|ne|[gl][et])_[0-9]{4}(-[0-9]{2}){2}")

for entity_type in ALL_FIELDS.keys():
    assert set(ALL_FIELDS[entity_type]) >= set(INEQUALITY_COMPARABLE_FIELDS[entity_type])
    assert set(ALL_FIELDS[entity_type]) >= set(EQUALITY_COMPARABLE_FIELDS[entity_type])
    assert set(ALL_FIELDS[entity_type]) >= set(SORTABLE_FIELDS[entity_type])
    assert set(EQUALITY_COMPARABLE_FIELDS[entity_type]) >= set(INEQUALITY_COMPARABLE_FIELDS[entity_type])
