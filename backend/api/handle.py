from .schema import Bill, Congressperson, Committee, db
from .consts import *
from flask import jsonify
from sqlalchemy import func, and_, or_
from werkzeug.datastructures import MultiDict
import re

def properties_request(entity_type):
    return [
        {
            "api_name": field,
            "display_name": DISPLAY_NAMES[entity_type][field],
            "type": FIELD_TYPES[entity_type][field],
            "sortable": field in SORTABLE_FIELDS[entity_type],
            "equality_filterable": field in EQUALITY_COMPARABLE_FIELDS[entity_type],
            "inequality_filterable": field in INEQUALITY_COMPARABLE_FIELDS[entity_type],
        } for field in ALL_FIELDS[entity_type]
    ]

def bill_request(bill_id):
    """
    Get the requested bill from the DB.

    :param bill_id: the ID of the requested bill (positive integer)
    :return: JSON response containing the bill data, or 404 response
    """
    return Bill.query.get_or_404(bill_id).serialize()

def congressperson_request(congressperson_id):
    """
    Get the requested congressperson from the DB.

    :param congressperson_id: the ID of the requested congressperson (positive integer)
    :return: JSON response containing the congressperson data, or 404 response
    """
    return Congressperson.query.get_or_404(congressperson_id).serialize()

def committee_request(committee_id):
    """
    Get the requested committee from the DB.

    :param committee_id: the ID of the requested committee (positive integer)
    :return: JSON response containing the committee data, or 404 response
    """
    return Committee.query.get_or_404(committee_id).serialize()

def bills_request(url_params):
    """
    Get the requested page of bills from the DB, possibly sorted.

    :param url_params: URL param multi-dict, e.g. request.args
    :return: JSON response containing the bill data and pagination metadata, or error response
    """
    return list_request(url_params, "bill")

def congresspeople_request(url_params):
    """
    Get the requested page of congresspeople from the DB, possibly sorted.

    :param url_params: URL param multi-dict, e.g. request.args
    :return: JSON response containing the congressperson data and pagination metadata, or error response
    """
    return list_request(url_params, "congressperson")

def committees_request(url_params):
    """
    Get the requested page of committees from the DB, possibly sorted.

    :param url_params: URL param multi-dict, e.g. request.args
    :return: JSON response containing the committee data and pagination metadata, or error response
    """
    return list_request(url_params, "committee")

def first_element(x):
    if hasattr(x, "__iter__"):
        return x[0]
    return x

def list_request(url_params, entity_type, global_search=False):
    """
    Fetch a "page" of items from the DB and pack them alongside request metadata.
    Does pagination and sorting.

    :param url_params: URL param multi-dict, e.g. request.args
    :param entity_type: Bill | Congressperson | Committee class (not object)
    :return: JSON response containing pagination metadata and the item list payload, or error response
    """
    # parse URL parameters, abort if errors
    params, where, having, fields, association_tables, errors = validate(url_params, entity_type)
    if len(errors) != 0:
        if global_search:
            return {"errors": errors}, 400
        return jsonify({"errors": errors}), 400
    # add any needed query elements if needed for COUNT() fields, compute the order by direction
    field = ALL_FIELDS[entity_type][params["sort_by"]]
    if type(field) == tuple:
        fk, association_table = field
        field = func.count(distinct(fk))

        if field not in fields:
            fields.append(field)
        if association_table not in association_tables:
            association_tables.append(association_table)
    order_by = field.asc() if params["sort_ascending"] else field.desc()

    # build and execute the query
    try:
        query = db.session\
            .query(TABLES[entity_type], *fields)
        for association_table in association_tables:
            query = query.join(association_table, isouter=True)
        query = query\
            .where(and_(*where))\
            .group_by(TABLES[entity_type].id)\
            .having(and_(*having))\
            .order_by(order_by)
        # print(query.statement.compile(compile_kwargs={"literal_binds": True})) # the raw SQL statement
        res = query\
            .paginate(page=params["page"], per_page=params["items_per_page"], error_out=False)
    except Exception as e:
        if global_search:
            return {"errors": [str(e)]}, 500
        return jsonify({"errors": [str(e)]}), 500

    # build and return the JSON response
    items = [first_element(item).serialize() for item in res.items]
    j = {
        "page": res.page,
        "items_per_page": res.per_page,
        "total_items": res.total,
        "items_this_page": len(res.items),
        "sort_by": params["sort_by"],
        "sort_ascending": params["sort_ascending"],
        "items": items,
    }
    if global_search:
        return j
    return jsonify(j)

def validate(url_params, entity_type):
    """
    Validate URL parameters. If all are valid, parse and type-coerce them.
    Otherwise, return non-empty list of errors.

    :param url_params: URL param multi-dict, e.g. request.args
    :param entity_type: Bill | Congressperson | Committee class (not object)
    :return: parsed URL parameters and associated query elements if no errors (else undefined), list of errors
    """
    errors = []
    params = DEFAULTS.copy()

    for k, v in params.items():
        try:
            params[k] = url_params.get(k, v, TYPES[k])

            if k in ["page", "items_per_page"] and params[k] < 1:
                errors.append(f"value {k} must be positive integers")
            elif k == "sort_by":
                if params[k] in ["", None]:
                    params[k] = "id"
                elif params[k] not in SORTABLE_FIELDS[entity_type]:
                    errors.append(f"value {url_params.get(k)} is not a sortable field")
            elif k == "search_conjunction" and params[k].lower() not in ["and", "or"]:
                errors.append(f"value {k} must be one of 'and' or 'or'")
        except ValueError:
            errors.append(f"invalid {k} value ({url_params.get(k)})")

    where, having, fields, association_tables, filter_errors = validate_filters(url_params, entity_type)
    where.append(create_search_predicate(params["search"], params["search_conjunction"], entity_type))
    errors += filter_errors
    return params, where, having, fields, association_tables, errors

def validate_filters(url_params, entity_type):
    """
    Validate filter URL parameters. If all are valid, parse them and produce
    filter operations for each. Otherwise, return non-empty list of errors.

    :param url_params: URL param multi-dict, e.g. request.args
    :param entity_type: Bill | Congressperson | Committee class (not object)
    :return: parsed URL parameters and associated query elements if no errors (else undefined), list of errors
    """
    where = [] # WHERE clause predicates
    having = [] # HAVING clause predicates
    fields = [] # fields to SELECT in addition to the entity
    association_tables = [] # tables to LEFT JOIN
    errors = []

    for k in url_params.keys():
        vs = url_params.getlist(k)
        if k in DATE_FIELDS[entity_type]:
            pattern = DATE_FIELD_PATTERN
        elif k in INEQUALITY_COMPARABLE_FIELDS[entity_type]:
            pattern = INEQUALITY_COMPARABLE_FIELD_PATTERN
        elif k in EQUALITY_COMPARABLE_FIELDS[entity_type]:
            pattern = EQUALITY_COMPARABLE_FIELD_PATTERN
        elif k in DEFAULTS:
            continue
        else:
            errors.append(f"invalid parameter '{k}'")
            continue

        first_time = True
        for v in vs:
            if not re.fullmatch(pattern, v):
                errors.append(f"invalid {k} filter value {v}. did you mean '{k}=eq_{v}'?")
                continue
            op, val = v.split("_")
            field = ALL_FIELDS[entity_type][k]

            if type(field) == tuple: # total_* fields
                fk, association_table = field
                field = func.count(distinct(fk))
                having.append(OPERATORS[op](field, val))
                if first_time:
                    first_time = False
                    fields.append(field)
                    association_tables.append(association_table)
            else: # all others
                where.append(OPERATORS[op](field, val))

    return where, having, fields, association_tables, errors

def create_search_predicate(search_str, search_conjunction_str, entity_type):
    """
    Create query predicate in which at least one (or) or every (and) searched token
    must be contained in some searchable (i.e. string) field.

    :param search_str: the search string
    :param search_conjunction_str: "and" or "or"
    :param entity_type: Bill | Congressperson | Committee class (not object)
    :return: a conjunction (and) or disjunction (or) over all tokens of disjunctions over all searchable fields
    """
    search_conjunction = {"and": and_, "or": or_}[search_conjunction_str.lower()]
    tokens = search_str.split()

    search = []
    for token in tokens:
        predicates = []
        for field in ALL_FIELDS[entity_type]:
            if FIELD_TYPES[entity_type][field] == "string":
                column = ALL_FIELDS[entity_type][field]
                predicates.append(column.contains(token))
        search.append(or_(*predicates))

    return search_conjunction(*search)

def search_request(url_params):
    """
    Wrap list_request by making queries for lists of each model type,
    then wrap those in one JSON response. Note that error responses
    are per-model, so it may be that only some models have errors.
    
    :param url_params: URL param multi-dict, e.g. request.args
    :return: the wrapped JSON responses
    """
    j = {}
    response_code = 200
    for entity_type in ["bill", "congressperson", "committee"]:
        url_params_for_entity_type = MultiDict()
        for k, v in url_params.items():
            if k != "sort_by" and (k in DEFAULTS or k in EQUALITY_COMPARABLE_FIELDS[entity_type]):
                url_params_for_entity_type.add(k, v)
        print(entity_type)
        print(url_params_for_entity_type)
        res = list_request(url_params_for_entity_type, entity_type, global_search=True)
        if type(res) == tuple:
            j[entity_type], response_code = res
        else:
            j[entity_type] = res
    return jsonify(j), response_code
