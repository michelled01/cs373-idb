import os
from typing import List
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import select, func, Column, ForeignKey, Table, Integer, Boolean, Text
from sqlalchemy.orm import relationship, mapped_column, Mapped
from api import app

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DB_CONNECTION_STRING', 'mysql://mysql:mysql@localhost:3306/legislore')
db = SQLAlchemy(app)

# many to many
    
votes_yea = Table("votes_yea", db.Model.metadata,
    Column("bill_id", Integer, ForeignKey("bills.id"), primary_key=True),
    Column("congressperson_id", Integer, ForeignKey("congresspeople.id"), primary_key=True),
)

votes_nay = Table("votes_nay", db.Model.metadata,
    Column("bill_id", Integer, ForeignKey("bills.id"), primary_key=True),
    Column("congressperson_id", Integer, ForeignKey("congresspeople.id"), primary_key=True),
)

sponsorings = Table("sponsorings", db.Model.metadata,
    Column("bill_id", Integer, ForeignKey("bills.id"), primary_key=True),
    Column("congressperson_id", Integer, ForeignKey("congresspeople.id"), primary_key=True)
)

members = Table("members", db.Model.metadata,
    Column("committee_id", Integer, ForeignKey("committees.id"), primary_key=True),
    Column("congressperson_id", Integer, ForeignKey("congresspeople.id"), primary_key=True)
)

actions_by_committee = Table("actions_by_committee", db.Model.metadata,
    Column("committee_id", Integer, ForeignKey("committees.id"), primary_key=True),
    Column("bill_id", Integer, ForeignKey("bills.id"), primary_key=True)
)

# one to many

class Action(db.Model):
    """
    Represents an action on a bill, along with the date of that action.
    The sequence number is used to order actions because there may be multiple
    actions on a given bill on the same day.
    """
    __tablename__ = "actions_on_bill"

    bill_id: Mapped[int] = mapped_column(ForeignKey("bills.id"), primary_key=True)
    bill: Mapped["Bill"] = relationship(back_populates="actions")

    sequence_number = Column(Integer, primary_key=True)
    action = Column(Text)
    date = Column(Text)

# the 3 object types

class Bill(db.Model):
    """
    Represents a bill.
    """
    __tablename__ = "bills"

    id: Mapped[int] = mapped_column(primary_key=True)

    type = Column(Text, nullable=False)
    number = Column(Integer)
    title = Column(Text)
    description = Column(Text)
    congress = Column(Integer)
    chamber = Column(Text)
    subject_area = Column(Text)
    full_text_link = Column(Text)

    actions: Mapped[List["Action"]] = relationship(back_populates="bill")

    committees: Mapped[List["Committee"]] = relationship(secondary=actions_by_committee, back_populates="bills")
    sponsors: Mapped[List["Congressperson"]] = relationship(secondary=sponsorings, back_populates="sponsored_bills")
    voted_yea: Mapped[List["Congressperson"]] = relationship(secondary=votes_yea, back_populates="bills_voted_yea")
    voted_nay: Mapped[List["Congressperson"]] = relationship(secondary=votes_nay, back_populates="bills_voted_nay")

    def serialize(self):
        """
        Create a JSON representation of the bill.

        :return: the JSON representation
        """
        return {
            "id": self.id,
            "name": {
                "type": self.type,
                "number": self.number,
                "title": self.title,
            },
            "description": self.description,
            "congress": self.congress,
            "chamber": self.chamber,
            "subject_area": self.subject_area,
            "full_text_link": self.full_text_link,
            "actions": list(map(
                lambda row : {"action": row[0], "date": row[1]},
                db.session.query(Action.action, Action.date, Action.sequence_number)
                    .where(Action.bill_id == self.id)
                    .order_by(Action.sequence_number)
                    .all()
            )),
            "relations": {
                "total_committees": db.session.query(func.count(Bill.id)).join(Bill.committees).where(Bill.id == self.id).scalar(),
                "total_sponsors": db.session.query(func.count(Bill.id)).join(Bill.sponsors).where(Bill.id == self.id).scalar(),
                "total_votes_yea": db.session.query(func.count(Bill.id)).join(Bill.voted_yea).where(Bill.id == self.id).scalar(),
                "total_votes_nay": db.session.query(func.count(Bill.id)).join(Bill.voted_nay).where(Bill.id == self.id).scalar(),
                "committees": [p.preview() for p in self.committees],
                "sponsors": [p.preview() for p in self.sponsors],
                "voted_yea": [p.preview() for p in self.voted_yea],
                "voted_nay": [p.preview() for p in self.voted_nay],
            },
        }
    
    def preview(self):
        """
        Create a JSON preview represenation of the bill.

        :return: the JSON preview representation
        """
        return {
            "id": self.id,
            "name": f"{self.type} {self.number} {self.title}",
        }
    
class Congressperson(db.Model):
    """
    Represents a congressperson.
    """
    __tablename__ = "congresspeople"

    id: Mapped[int] = mapped_column(primary_key=True)

    first_name = Column(Text)
    last_name = Column(Text)
    middle_initial = Column(Text)
    chamber = Column(Text)
    party = Column(Text)
    party_initial = Column(Text)
    district = Column(Text)
    state = Column(Text)
    state_abbreviation = Column(Text)
    image_url = Column(Text)
    website_url = Column(Text)

    chairing: Mapped[List["Committee"]] = relationship(back_populates="chair")

    committees: Mapped[List["Committee"]] = relationship(secondary=members, back_populates="members")
    sponsored_bills: Mapped[List["Bill"]] = relationship(secondary=sponsorings, back_populates="sponsors")
    bills_voted_yea: Mapped[List["Bill"]] = relationship(secondary=votes_yea, back_populates="voted_yea")
    bills_voted_nay: Mapped[List["Bill"]] = relationship(secondary=votes_nay, back_populates="voted_nay")

    def serialize(self):
        """
        Create a JSON representation of the congressperson.

        :return: the JSON representation
        """
        return {
            "id": self.id,
            "name": {
                "last": self.last_name,
                "first": self.first_name,
                "middle_initial": self.middle_initial,
            },
            "party": self.party,
            "party_initial": self.party_initial,
            "image_url": self.image_url,
            "website_url": self.website_url,
            "chamber" : self.chamber,
            "district": self.district,
            "state": self.state,
            "state_abbreviation": self.state_abbreviation,
            "relations": {
                "total_committees": db.session.query(func.count(Congressperson.id)).join(Congressperson.committees).where(Congressperson.id == self.id).scalar(),
                "total_sponsored_bills": db.session.query(func.count(Congressperson.id)).join(Congressperson.sponsored_bills).where(Congressperson.id == self.id).scalar(),
                "total_votes_yea": db.session.query(func.count(Congressperson.id)).join(Congressperson.bills_voted_yea).where(Congressperson.id == self.id).scalar(),
                "total_votes_nay": db.session.query(func.count(Congressperson.id)).join(Congressperson.bills_voted_nay).where(Congressperson.id == self.id).scalar(),
                "committees": [p.preview() for p in self.committees],
                "sponsored_bills": [p.preview() for p in self.sponsored_bills],
                "bills_voted_yea": [p.preview() for p in self.bills_voted_yea],
                "bills_voted_nay": [p.preview() for p in self.bills_voted_nay],
            },
        }
    
    def preview(self):
        """
        Create a JSON preview represenation of the bill.

        :return: the JSON preview representation
        """
        return {
            "id": self.id,
            "name": f"{'Rep.' if self.chamber == 'House' else 'Sen.'} {self.first_name}{f' {self.middle_initial}' if self.middle_initial else ''} {self.last_name} [{self.party_initial}-{self.state_abbreviation}]",
        }

class Committee(db.Model):
    """
    Represents a committee.
    """
    __tablename__ = "committees"

    id: Mapped[int] = mapped_column(primary_key=True)

    name = Column(Text)
    type = Column(Text)
    chamber = Column(Text)
    established_date = Column(Text)
    active = Column(Boolean)
    website_url = Column(Text)

    congressperson_id: Mapped[int] = mapped_column(ForeignKey("congresspeople.id"))
    chair: Mapped["Congressperson"] = relationship(back_populates="chairing")

    members: Mapped[List["Congressperson"]] = relationship(secondary=members, back_populates="committees")
    bills: Mapped[List["Bill"]] = relationship(secondary=actions_by_committee, back_populates="committees")

    def serialize(self):
        """
        Create a JSON representation of the committee.

        :return: the JSON representation
        """
        return {
            "id": self.id,
            "name": self.name,
            "type": self.type,
            "chamber": self.chamber,
            "established_date": self.established_date,
            "active": self.active,
            "website_url": self.website_url,
            "relations": {
                "total_members": db.session.query(func.count(Committee.id)).join(Committee.members).where(Committee.id == self.id).scalar(),
                "total_bills": db.session.query(func.count(Committee.id)).join(Committee.bills).where(Committee.id == self.id).scalar(),
                "chair": self.chair.preview(),
                "members": [p.preview() for p in self.members],
                "bills": [p.preview() for p in self.bills],
            },
        }
    
    def preview(self):
        """
        Create a JSON preview represenation of the bill.

        :return: the JSON preview representation
        """
        return {
            "id": self.id,
            "name": self.name,
        }
    
