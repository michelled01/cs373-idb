import os
import json
from api import app
from .gitlab import get_counts, get_links
from . import handle
from . import consts
from flask import request
import json

j = json.load(open(os.path.dirname(__file__) + '/../scrape/dummy_data.json'))

@app.route("/")
def index():
    """
    Welcome message. Used to test whether the API is up.

    :return: the welcome message (HTTP 200)
    """
    return "Welcome to the LegisLore API."

@app.route("/api")
@app.route("/api.json")
def api():
    """
    Respond with a list of all endpoints on the API.

    :return: JSON response containing all available endpoints
    """
    return {
        "available_endpoints": [
            "/",
            "/api",
            "/api/gitlab_stats",
            "/api/bill_properties",
            "/api/bill/<int:id>",
            "/api/bills",
            "/api/congressperson_properties",
            "/api/congressperson/<int:id>",
            "/api/congresspeople",
            "/api/committee_properties",
            "/api/committee/<int:id>",
            "/api/committees",
            "/api/search"
        ]
    }

@app.route("/api/search")
@app.route("/api/search.json")
def search():
    return handle.search_request(request.args)

@app.route("/api/gitlab_stats")
@app.route("/api/gitlab_stats.json")
def gitlab_stats():
    """
    Respond with the GitLab repo statistics.

    :return: JSON response containing the statistics, or an error response
    """
    try:
        total_commits, total_issues, total_unit_tests, commits, issues, unit_tests = get_counts()
        gitlab_repo_url, gitlab_wiki_url, gitlab_issue_tracker_url = get_links()
    except Exception as e:
        return {"error": str(e)}, 500
    return {
        "total_commits": total_commits,
        "total_issues": total_issues,
        "total_unit_tests": total_unit_tests,
        "commits": commits,
        "issues": issues,
        "unit_tests": unit_tests,
        "gitlab_repo_url": gitlab_repo_url,
        "gitlab_wiki_url": gitlab_wiki_url,
        "gitlab_issue_tracker_url": gitlab_issue_tracker_url,
        "postman_url": "https://documenter.getpostman.com/view/16329964/2s93z6djFD",
    }

@app.route("/api/bill_properties")
@app.route("/api/bill_properties.json")
def bill_properties():
    return handle.properties_request("bill")

@app.route("/api/congressperson_properties")
@app.route("/api/congressperson_properties.json")
def congressperson_properties():
    return handle.properties_request("congressperson")

@app.route("/api/committee_properties")
@app.route("/api/committee_properties.json")
def committee_properties():
    return handle.properties_request("committee")

@app.route("/api/bill/<int:bill_id>")
@app.route("/api/bill/<int:bill_id>.json")
def bill(bill_id):
    """
    Respond with the requested bill data.

    :param bill_id: the ID of the requested bill
    :return: JSON response containing the bill data, or an error response
    """
    try:
        return handle.bill_request(bill_id)
    except Exception as e:
        return {"error": str(e)}, 500

@app.route("/api/bills")
@app.route("/api/bills.json")
def bills():
    """
    Respond with the requested page of bill data, along with pagination metadata.

    :param page: The page of items to respond with. Defaults to 1. Must be > 0.
    :param items_per_page: The number of items to respond with per page. Defaults to 10. If fewer than `items_per_page` items are available for the requested `page`, only that many items are returned. Must be > 0. 
    :param sort_by: The field by which to order items. Defaults to null or the empty string, which corresponds to a undefined ordering of the items. See the Postman documentation for this endpoint for the full list of sortable fields.
    :param sort_ascending: If the results are to be sorted, whether they are to be sorted in ascending order (true) or descending order (false). Defaults to true.
    :return: JSON response containing the page, or an error response
    """
    try:
        return handle.bills_request(request.args)
    except Exception as e:
        return {"error": str(e)}, 500

@app.route("/api/congressperson/<int:congressperson_id>")
@app.route("/api/congressperson/<int:congressperson_id>.json")
def congressperson(congressperson_id):
    """
    Respond with the requested congressperson data.

    :param congressperson_id: the ID of the requested congressperson
    :return: JSON response containing the congressperson data, or an error response
    """
    try:
        return handle.congressperson_request(congressperson_id)
    except Exception as e:
        return {"error": str(e)}, 500

@app.route("/api/congresspeople")
@app.route("/api/congresspeople.json")
def congresspeople():
    """
    Respond with the requested page of congressperson data, along with pagination metadata.

    :param page: The page of items to respond with. Defaults to 1. Must be > 0.
    :param items_per_page: The number of items to respond with per page. Defaults to 10. If fewer than `items_per_page` items are available for the requested `page`, only that many items are returned. Must be > 0. 
    :param sort_by: The field by which to order items. Defaults to null or the empty string, which corresponds to a undefined ordering of the items. See the Postman documentation for this endpoint for the full list of sortable fields.
    :param sort_ascending: If the results are to be sorted, whether they are to be sorted in ascending order (true) or descending order (false). Defaults to true.
    :return: JSON response containing the page, or an error response
    """
    try:
        return handle.congresspeople_request(request.args)
    except Exception as e:
        return {"error": str(e)}, 500

@app.route("/api/committee/<int:committee_id>")
@app.route("/api/committee/<int:committee_id>.json")
def committee(committee_id):
    """
    Respond with the requested committee data.

    :param committee_id: the ID of the requested committee
    :return: JSON response containing the committee data, or an error response
    """
    try:
        return handle.committee_request(committee_id)
    except Exception as e:
        return {"error": str(e)}, 500

@app.route("/api/committees")
@app.route("/api/committees.json")
def committees():
    """
    Respond with the requested page of committee data, along with pagination metadata.

    :param page: The page of items to respond with. Defaults to 1. Must be > 0.
    :param items_per_page: The number of items to respond with per page. Defaults to 10. If fewer than `items_per_page` items are available for the requested `page`, only that many items are returned. Must be > 0. 
    :param sort_by: The field by which to order items. Defaults to null or the empty string, which corresponds to a undefined ordering of the items. See the Postman documentation for this endpoint for the full list of sortable fields.
    :param sort_ascending: If the results are to be sorted, whether they are to be sorted in ascending order (true) or descending order (false). Defaults to true.
    :return: JSON response containing the page, or an error response
    """
    try:
        return handle.committees_request(request.args)
    except Exception as e:
        return {"error": str(e)}, 500
