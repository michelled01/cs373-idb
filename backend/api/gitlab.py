from copy import deepcopy
import gitlab
import os
from dotenv import load_dotenv
from cachetools import cached, TTLCache

load_dotenv()

REPO_URL = "https://gitlab.com/michelled01/cs373-idb"
GITLAB_URL = os.environ.get("GITLAB_URL", "https://gitlab.com/michelled01/")
PROJECT_ID = os.environ.get("PROJECT_ID", "46969318")
PRIVATE_TOKEN = os.environ.get("PRIVATE_TOKEN")

gl = gitlab.Gitlab(private_token=PRIVATE_TOKEN)
project = gl.projects.get(PROJECT_ID)

COUNTS = {
    "Michelle Ding": 0,
    "Isaac Hammer": 0,
    "Jonathan Chacko": 0,
    "Tim Jones": 0,
    "Patrick Cammarata": 0,
}

def coerce_name(who):
    """
    Coerces names into a standard form. Necessary because the same person
    may commit under two different accounts/names.

    :param who: the user's name
    """
    if who == "Michelle":
        who = "Michelle Ding"
    if who == "patrick861":
        who = "Patrick Cammarata"
    return who

@cached(cache=TTLCache(maxsize=1, ttl=60 * 20))
def get_counts():
    """
    Get the total and per-user counts of commits, issues, and unit tests on the repo.

    :return: the total and per-user counts
    """
    commits = deepcopy(COUNTS)
    commits_iterator = project.commits.list(get_all=True, all=True, iterator=True)
    total_commits = 0
    for commit in commits_iterator:
        who = coerce_name(commit.asdict()["committer_name"])
        commits[who] += 1
        total_commits += 1
    
    issues = deepcopy(COUNTS)
    issue_iterator = project.issues.list(get_all=True, all=True, iterator=True)
    total_issues = 0
    for issue in issue_iterator:
        who = coerce_name(issue.asdict()["author"]["name"])
        issues[who] += 1
        total_issues += 1
    
    unit_tests = deepcopy(COUNTS)
    unit_tests["Isaac Hammer"] = 9
    total_unit_tests = 9
    return total_commits, total_issues, total_unit_tests, commits, issues, unit_tests

def get_links():
    """
    Get the links to the GitLab repo, wiki, and issue tracker.

    :return: the links
    """
    gitlab_repo_url = REPO_URL
    gitlab_wiki_url = f"{gitlab_repo_url}/-/wikis/LegisLore-Technical-Report"
    gitlab_issue_tracker_url = f"{gitlab_repo_url}/-/issues"
    return gitlab_repo_url, gitlab_wiki_url, gitlab_issue_tracker_url



