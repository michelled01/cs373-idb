import api
from api import app

if __name__ == "__main__":
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    app.run(debug=True)
else:
    # cache responses in production
    @app.after_request
    def add_header(response):
        response.cache_control.max_age = 3600
        return response