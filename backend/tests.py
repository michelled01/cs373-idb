from unittest import main, TestCase
from api.schema import Committee, Congressperson, Bill, Action, db, app

class ModelTestCase(TestCase):
    @classmethod
    def tearDownClass(cls):
        pass

    @classmethod
    def setUpClass(cls):
        app.app_context().push()

        db.drop_all()
        db.create_all()

        committee_1 = Committee(
            id=1,
            name="First Committee",
            type="Standing",
            chamber="House",
            established_date="2023-01-01",
            active=True,
            website_url="https://first-committee.gov",
            congressperson_id=1,
        )
        committee_2 = Committee(
            id=2,
            name="Second Committee",
            type="Select",
            chamber="Senate",
            established_date="2022-01-01",
            active=True,
            website_url="https://second-committee.gov",
            congressperson_id=1,
        )
        committee_3 = Committee(
            id=3,
            name="Third Committee",
            type="Other",
            chamber="Joint",
            established_date="2021-01-01",
            active=True,
            website_url="https://third-committee.gov",
            congressperson_id=2,
        )
        congressperson_1 = Congressperson(
            id=1,
            first_name = "First",
            last_name = "Congressman",
            middle_initial = "X",
            chamber = "House",
            party = "Democrat",
            party_initial = "D",
            district = 1,
            state = "Texas",
            state_abbreviation = "TX",
            image_url = "https://members.house.gov/first-congressman/headshot.jpg",
            website_url = "https://members.house.gov/first-congressman",
        )
        congressperson_2 = Congressperson(
            id=2,
            first_name = "Second",
            last_name = "Congresswoman",
            middle_initial = "X",
            chamber = "Senate",
            party = "Republican",
            party_initial = "R",
            district = None,
            state = "California",
            state_abbreviation = "CA",
            image_url = "https://members.house.gov/second-congresswoman/headshot.jpg",
            website_url = "https://members.house.gov/second-congresswoman",
        )
        congressperson_3 = Congressperson(
            id=3,
            first_name = "Third",
            last_name = "Congressperson",
            middle_initial = "X",
            chamber = "House",
            party = "Independent",
            party_initial = "I",
            district = 1,
            state = "District of Columbia",
            state_abbreviation = "DC",
            image_url = "https://members.house.gov/third-congressperson/headshot.jpg",
            website_url = "https://members.house.gov/third-congressperson",
        )
        bill_1 = Bill(
            id=1,
            type = "HR",
            number = 1,
            title = "First Bill",
            description = None,
            congress = 118,
            chamber = "House",
            subject_area = None,
            full_text_link = "https://legislore.me/full_text/1.xml",
        )
        bill_2 = Bill(
            id=2,
            type = "S",
            number = 2,
            title = "Second Bill",
            description = None,
            congress = 118,
            chamber = "House",
            subject_area = "Meta",
            full_text_link = "https://legislore.me/full_text/2.xml",
        )
        bill_3 = Bill(
            id=3,
            type = "HCONRES",
            number = 3,
            title = "First Bill",
            description = None,
            congress = 118,
            chamber = "House",
            subject_area = "Examples",
            full_text_link = "https://legislore.me/full_text/3.xml",
        )
        bill_1.actions.append(Action(
            bill_id=bill_1.id,
            bill=bill_1,
            sequence_number=0,
            date="2023-02-03",
            action="Nothing happened."
        ))
        bill_1.actions.append(Action(
            bill_id=bill_1.id,
            bill=bill_1,
            sequence_number=1,
            date="2023-02-03",
            action="Something happened."
        ))
        bill_2.actions.append(Action(
            bill_id=bill_2.id,
            bill=bill_2,
            sequence_number=0,
            date="2023-01-03",
            action="Nothing happened."
        ))

        db.session.add_all([bill_1, bill_2, bill_3])
        db.session.commit()
        db.session.add_all([congressperson_1, congressperson_2, congressperson_3])
        db.session.commit()
        db.session.add_all([committee_1, committee_2, committee_3])
        db.session.commit()
        
        with db.session.no_autoflush:
            bill_1.committees.append(committee_1)
            bill_1.committees.append(committee_2)
            bill_2.committees.append(committee_2)
            bill_1.sponsors.append(congressperson_1)
            bill_1.sponsors.append(congressperson_2)
            bill_2.sponsors.append(congressperson_2)
            bill_1.voted_yea.append(congressperson_1)
            bill_1.voted_yea.append(congressperson_2)
            bill_2.voted_yea.append(congressperson_2)
            bill_1.voted_nay.append(congressperson_1)
            bill_1.voted_nay.append(congressperson_2)
            bill_2.voted_nay.append(congressperson_2)
        db.session.add_all([bill_1, bill_2, bill_3])
        db.session.commit()
        with db.session.no_autoflush:
            congressperson_1.sponsored_bills.append(bill_1)
            congressperson_2.sponsored_bills.append(bill_1)
            congressperson_2.sponsored_bills.append(bill_2)
            congressperson_1.bills_voted_yea.append(bill_1)
            congressperson_2.bills_voted_yea.append(bill_1)
            congressperson_2.bills_voted_yea.append(bill_2)
            congressperson_1.bills_voted_nay.append(bill_1)
            congressperson_2.bills_voted_nay.append(bill_1)
            congressperson_2.bills_voted_nay.append(bill_2)
            congressperson_1.committees.append(committee_1)
            congressperson_1.committees.append(committee_2)
            congressperson_2.committees.append(committee_3)
            congressperson_1.chairing.append(committee_1)
            congressperson_1.chairing.append(committee_2)
            congressperson_2.chairing.append(committee_3)
        db.session.add_all([congressperson_1, congressperson_2, congressperson_3])
        db.session.commit()
        with db.session.no_autoflush:
            committee_1.chair = congressperson_1
            committee_2.chair = congressperson_1
            committee_3.chair = congressperson_2
            committee_1.members.append(congressperson_1)
            committee_2.members.append(congressperson_1)
            committee_3.members.append(congressperson_2)
            committee_1.bills.append(bill_1)
            committee_2.bills.append(bill_1)
            committee_2.bills.append(bill_2)
        db.session.add_all([committee_1, committee_2, committee_3])
        db.session.commit()


    def test_bill_serialization(self):
        expectations = [
            {'id': 1, 'name': {'type': 'HR', 'number': 1, 'title': 'First Bill'}, 'description': None, 'congress': 118, 'chamber': 'House', 'subject_area': None, 'full_text_link': 'https://legislore.me/full_text/1.xml', 'actions': [{'action': 'Nothing happened.', 'date': '2023-02-03'}, {'action': 'Something happened.', 'date': '2023-02-03'}], 'relations': {'total_committees': 2, 'total_sponsors': 2, 'total_votes_yea': 2, 'total_votes_nay': 2, 'committees': [{'id': 1, 'name': 'First Committee'}, {'id': 2, 'name': 'Second Committee'}], 'sponsors': [{'id': 1, 'name': 'Rep. First X Congressman [D-TX]'}, {'id': 2, 'name': 'Sen. Second X Congresswoman [R-CA]'}], 'voted_yea': [{'id': 1, 'name': 'Rep. First X Congressman [D-TX]'}, {'id': 2, 'name': 'Sen. Second X Congresswoman [R-CA]'}], 'voted_nay': [{'id': 1, 'name': 'Rep. First X Congressman [D-TX]'}, {'id': 2, 'name': 'Sen. Second X Congresswoman [R-CA]'}]}},
            {'id': 2, 'name': {'type': 'S', 'number': 2, 'title': 'Second Bill'}, 'description': None, 'congress': 118, 'chamber': 'House', 'subject_area': 'Meta', 'full_text_link': 'https://legislore.me/full_text/2.xml', 'actions': [{'action': 'Nothing happened.', 'date': '2023-01-03'}], 'relations': {'total_committees': 1, 'total_sponsors': 1, 'total_votes_yea': 1, 'total_votes_nay': 1, 'committees': [{'id': 2, 'name': 'Second Committee'}], 'sponsors': [{'id': 2, 'name': 'Sen. Second X Congresswoman [R-CA]'}], 'voted_yea': [{'id': 2, 'name': 'Sen. Second X Congresswoman [R-CA]'}], 'voted_nay': [{'id': 2, 'name': 'Sen. Second X Congresswoman [R-CA]'}]}},
            {'id': 3, 'name': {'type': 'HCONRES', 'number': 3, 'title': 'First Bill'}, 'description': None, 'congress': 118, 'chamber': 'House', 'subject_area': 'Examples', 'full_text_link': 'https://legislore.me/full_text/3.xml', 'actions': [], 'relations': {'total_committees': 0, 'total_sponsors': 0, 'total_votes_yea': 0, 'total_votes_nay': 0, 'committees': [], 'sponsors': [], 'voted_yea': [], 'voted_nay': []}},
        ]
        for expectation in expectations:
            with self.subTest(f"bill {expectation['id']}"):
                bill = db.session.query(Bill).where(Bill.id == expectation["id"]).first()
                self.assertEqual(bill.serialize(), expectation)

    def test_congressperson_serialization(self):
        expectations = [
            {'id': 1, 'name': {'last': 'Congressman', 'first': 'First', 'middle_initial': 'X'}, 'party': 'Democrat', 'party_initial': 'D', 'image_url': 'https://members.house.gov/first-congressman/headshot.jpg', 'website_url': 'https://members.house.gov/first-congressman', 'chamber': 'House', 'district': '1', 'state': 'Texas', 'state_abbreviation': 'TX', 'relations': {'total_committees': 2, 'total_sponsored_bills': 1, 'total_votes_yea': 1, 'total_votes_nay': 1, 'committees': [{'id': 1, 'name': 'First Committee'}, {'id': 2, 'name': 'Second Committee'}], 'sponsored_bills': [{'id': 1, 'name': 'HR 1 First Bill'}], 'bills_voted_yea': [{'id': 1, 'name': 'HR 1 First Bill'}], 'bills_voted_nay': [{'id': 1, 'name': 'HR 1 First Bill'}]}},
            {'id': 2, 'name': {'last': 'Congresswoman', 'first': 'Second', 'middle_initial': 'X'}, 'party': 'Republican', 'party_initial': 'R', 'image_url': 'https://members.house.gov/second-congresswoman/headshot.jpg', 'website_url': 'https://members.house.gov/second-congresswoman', 'chamber': 'Senate', 'district': None, 'state': 'California', 'state_abbreviation': 'CA', 'relations': {'total_committees': 1, 'total_sponsored_bills': 2, 'total_votes_yea': 2, 'total_votes_nay': 2, 'committees': [{'id': 3, 'name': 'Third Committee'}], 'sponsored_bills': [{'id': 1, 'name': 'HR 1 First Bill'}, {'id': 2, 'name': 'S 2 Second Bill'}], 'bills_voted_yea': [{'id': 1, 'name': 'HR 1 First Bill'}, {'id': 2, 'name': 'S 2 Second Bill'}], 'bills_voted_nay': [{'id': 1, 'name': 'HR 1 First Bill'}, {'id': 2, 'name': 'S 2 Second Bill'}]}},
            {'id': 3, 'name': {'last': 'Congressperson', 'first': 'Third', 'middle_initial': 'X'}, 'party': 'Independent', 'party_initial': 'I', 'image_url': 'https://members.house.gov/third-congressperson/headshot.jpg', 'website_url': 'https://members.house.gov/third-congressperson', 'chamber': 'House', 'district': '1', 'state': 'District of Columbia', 'state_abbreviation': 'DC', 'relations': {'total_committees': 0, 'total_sponsored_bills': 0, 'total_votes_yea': 0, 'total_votes_nay': 0, 'committees': [], 'sponsored_bills': [], 'bills_voted_yea': [], 'bills_voted_nay': []}},
        ]
        for expectation in expectations:
            with self.subTest(f"congressperson {expectation['id']}"):
                congressperson = db.session.query(Congressperson).where(Congressperson.id == expectation["id"]).first()
                self.assertEqual(congressperson.serialize(), expectation)

    def test_committee_serialization(self):
        expectations = [
            {'id': 1, 'name': 'First Committee', 'type': 'Standing', 'chamber': 'House', 'established_date': '2023-01-01', 'active': True, 'website_url': 'https://first-committee.gov', 'relations': {'total_members': 1, 'total_bills': 1, 'chair': {'id': 1, 'name': 'Rep. First X Congressman [D-TX]'}, 'members': [{'id': 1, 'name': 'Rep. First X Congressman [D-TX]'}], 'bills': [{'id': 1, 'name': 'HR 1 First Bill'}]}},
            {'id': 2, 'name': 'Second Committee', 'type': 'Select', 'chamber': 'Senate', 'established_date': '2022-01-01', 'active': True, 'website_url': 'https://second-committee.gov', 'relations': {'total_members': 1, 'total_bills': 2, 'chair': {'id': 1, 'name': 'Rep. First X Congressman [D-TX]'}, 'members': [{'id': 1, 'name': 'Rep. First X Congressman [D-TX]'}], 'bills': [{'id': 1, 'name': 'HR 1 First Bill'}, {'id': 2, 'name': 'S 2 Second Bill'}]}},
            {'id': 3, 'name': 'Third Committee', 'type': 'Other', 'chamber': 'Joint', 'established_date': '2021-01-01', 'active': True, 'website_url': 'https://third-committee.gov', 'relations': {'total_members': 1, 'total_bills': 0, 'chair': {'id': 2, 'name': 'Sen. Second X Congresswoman [R-CA]'}, 'members': [{'id': 2, 'name': 'Sen. Second X Congresswoman [R-CA]'}], 'bills': []}},
        ]
        for expectation in expectations:
            with self.subTest(f"committee {expectation['id']}"):
                committee = db.session.query(Committee).where(Committee.id == expectation["id"]).first()
                self.assertEqual(committee.serialize(), expectation)

    def test_bill_congressperson_relations(self):
        bill_1 = db.session.query(Bill).where(Bill.id == 1).first()
        bill_2 = db.session.query(Bill).where(Bill.id == 2).first()
        bill_3 = db.session.query(Bill).where(Bill.id == 3).first()
        congressperson_1 = db.session.query(Congressperson).where(Congressperson.id == 1).first()
        congressperson_2 = db.session.query(Congressperson).where(Congressperson.id == 2).first()
        congressperson_3 = db.session.query(Congressperson).where(Congressperson.id == 3).first()

        self.assertSetEqual(set(bill_1.sponsors), {congressperson_1, congressperson_2})
        self.assertSetEqual(set(bill_2.sponsors), {congressperson_2})
        self.assertSetEqual(set(bill_3.sponsors), set())
        self.assertSetEqual(set(bill_1.voted_yea), {congressperson_1, congressperson_2})
        self.assertSetEqual(set(bill_2.voted_yea), {congressperson_2})
        self.assertSetEqual(set(bill_3.voted_yea), set())
        self.assertSetEqual(set(bill_1.voted_nay), {congressperson_1, congressperson_2})
        self.assertSetEqual(set(bill_2.voted_nay), {congressperson_2})
        self.assertSetEqual(set(bill_3.voted_nay), set())
    
    def test_bill_committee_relations(self):
        bill_1 = db.session.query(Bill).where(Bill.id == 1).first()
        bill_2 = db.session.query(Bill).where(Bill.id == 2).first()
        bill_3 = db.session.query(Bill).where(Bill.id == 3).first()
        committee_1 = db.session.query(Committee).where(Committee.id == 1).first()
        committee_2 = db.session.query(Committee).where(Committee.id == 2).first()
        committee_3 = db.session.query(Committee).where(Committee.id == 3).first()

        self.assertSetEqual(set(bill_1.committees), {committee_1, committee_2})
        self.assertSetEqual(set(bill_2.committees), {committee_2})
        self.assertSetEqual(set(bill_3.committees), set())

    def test_congressperson_bill_relations(self):
        congressperson_1 = db.session.query(Congressperson).where(Congressperson.id == 1).first()
        congressperson_2 = db.session.query(Congressperson).where(Congressperson.id == 2).first()
        congressperson_3 = db.session.query(Congressperson).where(Congressperson.id == 3).first()
        bill_1 = db.session.query(Bill).where(Bill.id == 1).first()
        bill_2 = db.session.query(Bill).where(Bill.id == 2).first()
        bill_3 = db.session.query(Bill).where(Bill.id == 3).first()
    
        self.assertSetEqual(set(congressperson_1.sponsored_bills), {bill_1})
        self.assertSetEqual(set(congressperson_2.sponsored_bills), {bill_1, bill_2})
        self.assertSetEqual(set(congressperson_3.sponsored_bills), set())
        self.assertSetEqual(set(congressperson_1.bills_voted_yea), {bill_1})
        self.assertSetEqual(set(congressperson_2.bills_voted_yea), {bill_1, bill_2})
        self.assertSetEqual(set(congressperson_3.bills_voted_yea), set())
        self.assertSetEqual(set(congressperson_1.bills_voted_nay), {bill_1})
        self.assertSetEqual(set(congressperson_2.bills_voted_nay), {bill_1, bill_2})
        self.assertSetEqual(set(congressperson_3.bills_voted_nay), set())

    def test_congressperson_committee_relations(self):
        congressperson_1 = db.session.query(Congressperson).where(Congressperson.id == 1).first()
        congressperson_2 = db.session.query(Congressperson).where(Congressperson.id == 2).first()
        congressperson_3 = db.session.query(Congressperson).where(Congressperson.id == 3).first()
        committee_1 = db.session.query(Committee).where(Committee.id == 1).first()
        committee_2 = db.session.query(Committee).where(Committee.id == 2).first()
        committee_3 = db.session.query(Committee).where(Committee.id == 3).first()
    
        self.assertSetEqual(set(congressperson_1.committees), {committee_1, committee_2})
        self.assertSetEqual(set(congressperson_2.committees), {committee_3})
        self.assertSetEqual(set(congressperson_3.committees), set())

        self.assertSetEqual(set(congressperson_1.chairing), {committee_1, committee_2})
        self.assertSetEqual(set(congressperson_2.chairing), {committee_3})
        self.assertSetEqual(set(congressperson_3.chairing), set())

    def test_committee_bill_relations(self):
        committee_1 = db.session.query(Committee).where(Committee.id == 1).first()
        committee_2 = db.session.query(Committee).where(Committee.id == 2).first()
        committee_3 = db.session.query(Committee).where(Committee.id == 3).first()
        bill_1 = db.session.query(Bill).where(Bill.id == 1).first()
        bill_2 = db.session.query(Bill).where(Bill.id == 2).first()
        bill_3 = db.session.query(Bill).where(Bill.id == 3).first()
    
        self.assertSetEqual(set(committee_1.bills), {bill_1})
        self.assertSetEqual(set(committee_2.bills), {bill_1, bill_2})
        self.assertSetEqual(set(committee_3.bills), set())

    def test_committee_congressperson_relations(self):
        committee_1 = db.session.query(Committee).where(Committee.id == 1).first()
        committee_2 = db.session.query(Committee).where(Committee.id == 2).first()
        committee_3 = db.session.query(Committee).where(Committee.id == 3).first()
        congressperson_1 = db.session.query(Congressperson).where(Congressperson.id == 1).first()
        congressperson_2 = db.session.query(Congressperson).where(Congressperson.id == 2).first()
        congressperson_3 = db.session.query(Congressperson).where(Congressperson.id == 3).first()
    
        self.assertSetEqual(set(committee_1.members), {congressperson_1})
        self.assertSetEqual(set(committee_2.members), {congressperson_1})
        self.assertSetEqual(set(committee_3.members), {congressperson_2})

        self.assertEqual(committee_1.chair, congressperson_1)
        self.assertEqual(committee_2.chair, congressperson_1)
        self.assertEqual(committee_3.chair, congressperson_2)

if __name__ == "__main__":
    main()
