from bs4 import BeautifulSoup
from dotenv import load_dotenv
import os
import re
import json
from xml2dict import XmlDictConfig
from xml.etree import cElementTree as ElementTree
import requests
from requests.adapters import HTTPAdapter, Retry

# exponential backoff in case of ratelimits
retry_strategy = Retry(
    total = 10,
    backoff_factor = 30,
    status_forcelist = [429, 504]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
session = requests.Session()
session.mount("https://", adapter)
session.mount("http://", adapter)

load_dotenv()
CONGRESS_GOV_API_TOKEN = os.environ.get("CONGRESS_GOV_API_TOKEN") # https://api.congress.gov/sign-up/
CONGRESS_GOV_API_BASE_URL = "https://api.congress.gov/v3"
CONGRESS_GOV_API_BASE_PARAMS = {"format": "json", "api_key": CONGRESS_GOV_API_TOKEN}
DATA_PATH = os.path.dirname(__file__) + "/data/committee_memberships.json"
CONGRESSES = [118] # not sure how to make this work with committee memberships or congresspeople, probably just relevant for bills and votes

SENATE_COMMITTEE_URL_BASE = "https://www.senate.gov/general/committee_membership/committee_memberships_"
SENATE_COMMITTEE_URL_SUFFIX = ".htm"
SENATE_COMMITTEE_CODES = ["SSAF", "SSAP", "SSAS", "SSBK", "SSBU", "SSCM", "SSEG", "SSEV", "SSFI", "SSFR", "SSHR", "SSGA", "SLIA", "JSPR", "JSTX", "JSLC", "JSEC", "SSJU", "SSRA", "SLET", "SLIN", "SSSB", "SPAG", "SSVA"]

HOUSE_COMMITTEE_URL_BASE = "https://clerk.house.gov/committees/"
HOUSE_COMMITTEE_CODES = ["AG00", "AP00", "AS00", "BU00", "ED00", "IF00", "SO00", "BA00", "FA00", "HM00", "HA00", "JU00", "II00", "GO00", "RU00", "SY00", "SM00", "PW00", "VR00", "WM00", "JL00", "JP00", "IT00", "EC00", "IG00", "ZS00"]

SENATE_VOTES_URL = "https://www.senate.gov/legislative/LIS/roll_call_lists/vote_menu_118_1.xml"
SENATE_VOTES_URL_BASE = "https://www.senate.gov/legislative/LIS/roll_call_votes/vote1181/vote_118_1_"
SENATE_VOTES_URL_SUFFIX = ".xml"

HOUSE_VOTES_URL = "https://clerk.house.gov/Votes/MemberVotes"
HOUSE_VOTES_URL_BASE = "https://clerk.house.gov/Votes/"

class Progress(object):
    def __init__(self, total):
        self.total = total
        self.so_far = 0

    def __next__(self):
        if self.so_far > self.total:
            raise StopIteration
        self.so_far += 1
        return f"{self.so_far: >8}/{self.total} [{self.so_far / self.total:.2%}]"

def scrape_committee_memberships():
    print("[SCRAPE_COMMITTEE_MEMBERSHIPS]")

    senate_committee_memberships = {}
    progress = Progress(len(SENATE_COMMITTEE_CODES))
    for code in SENATE_COMMITTEE_CODES:
        print(next(progress), end=" ")
        url = f"{SENATE_COMMITTEE_URL_BASE}{code}{SENATE_COMMITTEE_URL_SUFFIX}"
        print(url)
        res = session.get(url)
        bs = BeautifulSoup(res.content, "html5lib")
        tbody = bs.find("tbody")
        members = []
        for td in tbody.findAll("td"):
            for c in td.contents:
                match = re.search(r".+?\(\w\w\)", str(c))
                if match:
                    members.append(match.group())
        chair = members[0]
        senate_committee_memberships[code] = {"chair": None, "members": None}
        senate_committee_memberships[code]["chair"] = chair
        senate_committee_memberships[code]["members"] = members

    house_committee_memberships = {}
    progress = Progress(len(HOUSE_COMMITTEE_CODES))
    for code in HOUSE_COMMITTEE_CODES:
        print(next(progress), end=" ")
        url = f"{HOUSE_COMMITTEE_URL_BASE}{code}"
        print(url)
        res = session.get(url)
        bs = BeautifulSoup(res.content, "html5lib")
        spans = bs.findAll("span", class_="name")
        members = []
        for span in spans:
            name_and_state = span.text
            internal_code = span.parent()[1]["href"][-7:]
            members.append({"name_and_state": name_and_state, "internal_code": internal_code})
        if len(members) == 0:
            house_committee_memberships[code] = None
            continue
        chair = members[0]
        house_committee_memberships[code] = {"chair": None, "members": None}
        house_committee_memberships[code]["chair"] = chair
        house_committee_memberships[code]["members"] = members

    return {
        "senate_committee_memberships": senate_committee_memberships,
        "house_committee_memberships": house_committee_memberships,
    }

if __name__ == "__main__":
    with open(DATA_PATH, "w") as fp:
        data = scrape_committee_memberships()
        print(f"Saving data to {DATA_PATH}...", end="")
        json.dump(data, fp)
    print("Done.")