from bs4 import BeautifulSoup
from dotenv import load_dotenv
import os
import re
import json
from xml2dict import XmlDictConfig
from xml.etree import cElementTree as ElementTree
import requests
from requests.adapters import HTTPAdapter, Retry

# exponential backoff in case of ratelimits
retry_strategy = Retry(
    total = 10,
    backoff_factor = 30,
    status_forcelist = [429, 504]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
session = requests.Session()
session.mount("https://", adapter)
session.mount("http://", adapter)

load_dotenv()
CONGRESS_GOV_API_TOKEN = os.environ.get("CONGRESS_GOV_API_TOKEN") # https://api.congress.gov/sign-up/
CONGRESS_GOV_API_BASE_URL = "https://api.congress.gov/v3"
CONGRESS_GOV_API_BASE_PARAMS = {"format": "json", "api_key": CONGRESS_GOV_API_TOKEN}
DATA_PATH = os.path.dirname(__file__) + "/data/all.json"
CONGRESSES = [118] # not sure how to make this work with committee memberships or congresspeople, probably just relevant for bills and votes

SENATE_COMMITTEE_URL_BASE = "https://www.senate.gov/general/committee_membership/committee_memberships_"
SENATE_COMMITTEE_URL_SUFFIX = ".htm"
SENATE_COMMITTEE_CODES = ["SSAF", "SSAP", "SSAS", "SSBK", "SSBU", "SSCM", "SSEG", "SSEV", "SSFI", "SSFR", "SSHR", "SSGA", "SLIA", "JSPR", "JSTX", "JSLC", "JSEC", "SSJU", "SSRA", "SLET", "SLIN", "SSSB", "SPAG", "SSVA"]

HOUSE_COMMITTEE_URL_BASE = "https://clerk.house.gov/committees/"
HOUSE_COMMITTEE_CODES = ["AG00", "AP00", "AS00", "BU00", "ED00", "IF00", "SO00", "BA00", "FA00", "HM00", "HA00", "JU00", "II00", "GO00", "RU00", "SY00", "SM00", "PW00", "VR00", "WM00", "JL00", "JP00", "IT00", "EC00", "IG00", "ZS00"]

SENATE_VOTES_URL = "https://www.senate.gov/legislative/LIS/roll_call_lists/vote_menu_118_1.xml"
SENATE_VOTES_URL_BASE = "https://www.senate.gov/legislative/LIS/roll_call_votes/vote1181/vote_118_1_"
SENATE_VOTES_URL_SUFFIX = ".xml"

HOUSE_VOTES_URL = "https://clerk.house.gov/Votes/MemberVotes"
HOUSE_VOTES_URL_BASE = "https://clerk.house.gov/Votes/"

class Progress(object):
    def __init__(self, total):
        self.total = total
        self.so_far = 0

    def __next__(self):
        if self.so_far > self.total:
            raise StopIteration
        self.so_far += 1
        return f"{self.so_far: >8}/{self.total} [{self.so_far / self.total:.2%}]"

def scrape_all(congress):
    print(f"[SCRAPE_ALL] congress={congress}")

    # from api.congress.gov
    bills = scrape_bills(congress)
    congresspeople = scrape_congresspeople()
    committees = scrape_committees(congress)

    # extracted from HTML
    committee_memberships = scrape_committee_memberships()
    votes = scrape_votes(congress)

    return {
        "bills": bills,
        "congresspeople": congresspeople,
        "committees": committees,
        "committee_memberships": committee_memberships,
        "votes": votes,
    }

def scrape_bills(congress):
    print(f"[SCRAPE_BILLS] congress={congress}")
    endpoint = f"{CONGRESS_GOV_API_BASE_URL}/bill/{congress}"
    previews = depaginate(endpoint, CONGRESS_GOV_API_BASE_PARAMS, "bills")
    urls = [preview["url"][:-12] for preview in previews]
    bills = []
    progress = Progress(len(urls))
    for url in urls:
        print(next(progress), end=" ")
        res = session.get(url, params=CONGRESS_GOV_API_BASE_PARAMS)
        if res.status_code != 200:
            raise Exception(f"status code was {res.status_code}")
        j = res.json()

        res = session.get(f"{url}/committees", params=CONGRESS_GOV_API_BASE_PARAMS)
        j["committees"] = res.json()["committees"]

        actions = depaginate(f"{url}/actions", CONGRESS_GOV_API_BASE_PARAMS, "actions")
        j["actions"] = actions

        bills.append(j)
    return bills

def scrape_congresspeople():
    print(f"[SCRAPE_CONGRESSPEOPLE]")
    endpoint = f"{CONGRESS_GOV_API_BASE_URL}/member"
    previews = depaginate(endpoint, CONGRESS_GOV_API_BASE_PARAMS, "members")
    urls = [preview["url"][:-12] for preview in previews]
    congresspeople = []
    progress = Progress(len(urls))
    for url in urls:
        print(next(progress), end=" ")
        print(url)
        res = session.get(url, params=CONGRESS_GOV_API_BASE_PARAMS)
        if res.status_code != 200:
            raise Exception(f"status code was {res.status_code}")
        congresspeople.append(res.json())
        break
    return congresspeople

def scrape_committees(congress):
    print(f"[SCRAPE_COMMITTEES] congress={congress}")
    endpoint = f"{CONGRESS_GOV_API_BASE_URL}/committee/{congress}"
    previews = depaginate(endpoint, CONGRESS_GOV_API_BASE_PARAMS, "committees")
    urls = [preview["url"][:-12] for preview in previews]
    committees = []
    progress = Progress(len(urls))
    for url in urls:
        print(next(progress), end=" ")
        print(url)
        res = session.get(url, params=CONGRESS_GOV_API_BASE_PARAMS)
        if res.status_code != 200:
            raise Exception(f"status code was {res.status_code}")
        committees.append(res.json())
    return committees

def scrape_committee_memberships():
    print("[SCRAPE_COMMITTEE_MEMBERSHIPS]")

    senate_committee_memberships = {}
    progress = Progress(len(SENATE_COMMITTEE_CODES))
    for code in SENATE_COMMITTEE_CODES:
        print(next(progress), end=" ")
        url = f"{SENATE_COMMITTEE_URL_BASE}{code}{SENATE_COMMITTEE_URL_SUFFIX}"
        print(url)
        res = session.get(url)
        bs = BeautifulSoup(res.content, "html5lib")
        tbody = bs.find("tbody")
        members = []
        for td in tbody.findAll("td"):
            for c in td.contents:
                match = re.search(r".+?\(\w\w\)", str(c))
                if match:
                    members.append(match.group())
        chair = members[0]
        senate_committee_memberships[code] = {"chair": None, "members": None}
        senate_committee_memberships[code]["chair"] = chair
        senate_committee_memberships[code]["members"] = members

    house_committee_memberships = {}
    progress = Progress(len(HOUSE_COMMITTEE_CODES))
    for code in HOUSE_COMMITTEE_CODES:
        print(next(progress), end=" ")
        url = f"{HOUSE_COMMITTEE_URL_BASE}{code}"
        print(url)
        res = session.get(url)
        bs = BeautifulSoup(res.content, "html5lib")
        spans = bs.findAll("span", class_="name")
        members = []
        for span in spans:
            name_and_state = span.text
            internal_code = span.parent()[1]["href"][-7:]
            members.append({"name_and_state": name_and_state, "internal_code": internal_code})
        if len(members) == 0:
            house_committee_memberships[code] = None
            continue
        chair = members[0]
        house_committee_memberships[code] = {"chair": None, "members": None}
        house_committee_memberships[code]["chair"] = chair
        house_committee_memberships[code]["members"] = members

    return {
        "senate_committee_memberships": senate_committee_memberships,
        "house_committee_memberships": house_committee_memberships,
    }

def scrape_votes(congress):
    print(f"[SCRAPE_VOTES] congress={congress}")
    senate_votes = {}
    root = ElementTree.XML(session.get(SENATE_VOTES_URL).content)
    xmldict = XmlDictConfig(root)
    senate_vote_numbers = [v['vote_number'] for v in xmldict['votes']['vote']]
    progress = Progress(len(senate_vote_numbers))
    for senate_vote_number in senate_vote_numbers:
        print(next(progress), end=" ")
        url = f"{SENATE_COMMITTEE_URL_BASE}{senate_vote_number}{SENATE_VOTES_URL_SUFFIX}"
        root = ElementTree.XML(session.get(url).content)
        xmldict = XmlDictConfig(root)
        senate_votes[senate_vote_number] = xmldict

    house_votes = {}
    house_vote_numbers = depaginate_house_vote_numbers()
    progress = Progress(len(house_vote_numbers))
    for house_vote_number in house_vote_numbers:
        print(next(progress), end=" ")
        url = f"{HOUSE_VOTES_URL_BASE}{house_vote_number}"
        res = session.get(url)
        bs = BeautifulSoup(res.content, "html5lib")
        votes = bs.select("#member-votes > tr > td[data-label=vote]")
        anchors = bs.select("#member-votes > tr > td[data-label=member] > a")
        internal_codes = [anchor["href"][32:] for anchor in anchors]
        house_votes[house_vote_number] = list(zip(internal_codes, votes))

    return {
        "senate_votes": senate_votes,
        "house_votes": house_votes,
    }

def depaginate_house_vote_numbers():
    print("[DEPAGINATE_HOUSE_VOTE_NUMBERS]")
    house_vote_numbers = []
    page = 1
    while True:
        res = session.get(HOUSE_VOTES_URL, params={"page": page})
        bs = BeautifulSoup(res.content, "html5lib")
        anchors = bs.select(".detail-button > a.btn-library")
        if len(anchors) == 0:
            return house_vote_numbers
        house_vote_numbers += [anchor["href"][7:] for anchor in anchors]
        page += 1

def depaginate(endpoint, url_params, inner_key):
    print(f"    [DEPAGINATE] endpoint={endpoint} url_params={url_params} inner_key={inner_key}")
    LIMIT = 250
    offset = 0
    items = []
    while True:
        print(f"        offset={offset}")
        res = session.get(endpoint, params=(url_params | {"limit": LIMIT, "offset": offset}))
        if res.status_code != 200:
            return items
        j = res.json()
        items_this_page = j[inner_key]
        if len(items_this_page) == 0:
            return items
        items += items_this_page
        if "pagination" in j.keys() and "next" not in j["pagination"]:
            return items
        offset += LIMIT

if __name__ == "__main__":
    with open(DATA_PATH, "w") as fp:
        data = []
        for congress in CONGRESSES:
            all = scrape_all(congress)
            data += all
        print(f"Saving data to {DATA_PATH}...", end="")
        json.dump(data, fp)
    print("Done.")