from dotenv import load_dotenv
import os
import json
import requests
from requests.adapters import HTTPAdapter, Retry

# exponential backoff in case of ratelimits
retry_strategy = Retry(
    total = 10,
    backoff_factor = 30,
    status_forcelist = [429, 504]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
session = requests.Session()
session.mount("https://", adapter)
session.mount("http://", adapter)

load_dotenv()
CONGRESS_GOV_API_TOKEN = os.environ.get("CONGRESS_GOV_API_TOKEN") # https://api.congress.gov/sign-up/
CONGRESS_GOV_API_BASE_URL = "https://api.congress.gov/v3"
CONGRESS_GOV_API_BASE_PARAMS = {"format": "json", "api_key": CONGRESS_GOV_API_TOKEN}
DATA_PATH = os.path.dirname(__file__) + "/data/bills.json"
CONGRESSES = [118] # not sure how to make this work with committee memberships or congresspeople, probably just relevant for bills and votes

SENATE_COMMITTEE_URL_BASE = "https://www.senate.gov/general/committee_membership/committee_memberships_"
SENATE_COMMITTEE_URL_SUFFIX = ".htm"
SENATE_COMMITTEE_CODES = ["SSAF", "SSAP", "SSAS", "SSBK", "SSBU", "SSCM", "SSEG", "SSEV", "SSFI", "SSFR", "SSHR", "SSGA", "SLIA", "JSPR", "JSTX", "JSLC", "JSEC", "SSJU", "SSRA", "SLET", "SLIN", "SSSB", "SPAG", "SSVA"]

HOUSE_COMMITTEE_URL_BASE = "https://clerk.house.gov/committees/"
HOUSE_COMMITTEE_CODES = ["AG00", "AP00", "AS00", "BU00", "ED00", "IF00", "SO00", "BA00", "FA00", "HM00", "HA00", "JU00", "II00", "GO00", "RU00", "SY00", "SM00", "PW00", "VR00", "WM00", "JL00", "JP00", "IT00", "EC00", "IG00", "ZS00"]

SENATE_VOTES_URL = "https://www.senate.gov/legislative/LIS/roll_call_lists/vote_menu_118_1.xml"
SENATE_VOTES_URL_BASE = "https://www.senate.gov/legislative/LIS/roll_call_votes/vote1181/vote_118_1_"
SENATE_VOTES_URL_SUFFIX = ".xml"

HOUSE_VOTES_URL = "https://clerk.house.gov/Votes/MemberVotes"
HOUSE_VOTES_URL_BASE = "https://clerk.house.gov/Votes/"

class Progress(object):
    def __init__(self, total):
        self.total = total
        self.so_far = 0

    def __next__(self):
        if self.so_far > self.total:
            raise StopIteration
        self.so_far += 1
        return f"{self.so_far: >8}/{self.total} [{self.so_far / self.total:.2%}]"

def scrape_bills(congress):
    print(f"[SCRAPE_BILLS] congress={congress}")
    endpoint = f"{CONGRESS_GOV_API_BASE_URL}/bill/{congress}"
    previews = depaginate(endpoint, CONGRESS_GOV_API_BASE_PARAMS, "bills")
    urls = [preview["url"][:-12] for preview in previews]
    bills = []
    progress = Progress(len(urls))
    for url in urls:
        print(next(progress), end=" ")
        res = session.get(url, params=CONGRESS_GOV_API_BASE_PARAMS)
        if res.status_code != 200:
            raise Exception(f"status code was {res.status_code}")
        j = res.json()

        res = session.get(f"{url}/committees", params=CONGRESS_GOV_API_BASE_PARAMS)
        j["committees"] = res.json()["committees"]

        actions = depaginate(f"{url}/actions", CONGRESS_GOV_API_BASE_PARAMS, "actions")
        j["actions"] = actions

        bills.append(j)
    return bills

def depaginate(endpoint, url_params, inner_key):
    print(f"    [DEPAGINATE] endpoint={endpoint} url_params={url_params} inner_key={inner_key}")
    LIMIT = 250
    offset = 0
    items = []
    while True:
        print(f"        offset={offset}")
        res = session.get(endpoint, params=(url_params | {"limit": LIMIT, "offset": offset}))
        if res.status_code != 200:
            return items
        j = res.json()
        items_this_page = j[inner_key]
        if len(items_this_page) == 0:
            return items
        items += items_this_page
        if "pagination" in j.keys() and "next" not in j["pagination"]:
            return items
        offset += LIMIT

if __name__ == "__main__":
    with open(DATA_PATH, "w") as fp:
        data = []
        for congress in CONGRESSES:
            all = scrape_bills(congress)
            data += all
        print(f"Saving data to {DATA_PATH}...", end="")
        json.dump(data, fp)
    print("Done.")