Scripts to fetch data from our source API(s) and to scrape data (such as voting records) from pages with no available API and then to populate the database.

Also contains manually acquired data for the scripts to use in populating the DB (such as committee website links).
