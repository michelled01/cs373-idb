from bs4 import BeautifulSoup
from dotenv import load_dotenv
import os
import re
import json
from xml2dict import XmlDictConfig
from xml.etree import cElementTree as ElementTree
import requests
from requests.adapters import HTTPAdapter, Retry

# exponential backoff in case of ratelimits
retry_strategy = Retry(
    total = 10,
    backoff_factor = 30,
    status_forcelist = [429, 504]
)
adapter = HTTPAdapter(max_retries=retry_strategy)
session = requests.Session()
session.mount("https://", adapter)
session.mount("http://", adapter)

load_dotenv()
CONGRESS_GOV_API_TOKEN = os.environ.get("CONGRESS_GOV_API_TOKEN") # https://api.congress.gov/sign-up/
CONGRESS_GOV_API_BASE_URL = "https://api.congress.gov/v3"
CONGRESS_GOV_API_BASE_PARAMS = {"format": "json", "api_key": CONGRESS_GOV_API_TOKEN}
DATA_PATH = os.path.dirname(__file__) + "/data/votes.json"
CONGRESSES = [118] # not sure how to make this work with committee memberships or congresspeople, probably just relevant for bills and votes

SENATE_COMMITTEE_URL_BASE = "https://www.senate.gov/general/committee_membership/committee_memberships_"
SENATE_COMMITTEE_URL_SUFFIX = ".htm"
SENATE_COMMITTEE_CODES = ["SSAF", "SSAP", "SSAS", "SSBK", "SSBU", "SSCM", "SSEG", "SSEV", "SSFI", "SSFR", "SSHR", "SSGA", "SLIA", "JSPR", "JSTX", "JSLC", "JSEC", "SSJU", "SSRA", "SLET", "SLIN", "SSSB", "SPAG", "SSVA"]

HOUSE_COMMITTEE_URL_BASE = "https://clerk.house.gov/committees/"
HOUSE_COMMITTEE_CODES = ["AG00", "AP00", "AS00", "BU00", "ED00", "IF00", "SO00", "BA00", "FA00", "HM00", "HA00", "JU00", "II00", "GO00", "RU00", "SY00", "SM00", "PW00", "VR00", "WM00", "JL00", "JP00", "IT00", "EC00", "IG00", "ZS00"]

SENATE_VOTES_URL = "https://www.senate.gov/legislative/LIS/roll_call_lists/vote_menu_118_1.xml"
SENATE_VOTES_URL_BASE = "https://www.senate.gov/legislative/LIS/roll_call_votes/vote1181/vote_118_1_"
SENATE_VOTES_URL_SUFFIX = ".xml"

HOUSE_VOTES_URL = "https://clerk.house.gov/Votes/MemberVotes"
HOUSE_VOTES_URL_BASE = "https://clerk.house.gov/Votes/"

class Progress(object):
    def __init__(self, total):
        self.total = total
        self.so_far = 0

    def __next__(self):
        if self.so_far > self.total:
            raise StopIteration
        self.so_far += 1
        return f"{self.so_far: >8}/{self.total} [{self.so_far / self.total:.2%}]"

def scrape_votes(congress):
    print(f"[SCRAPE_VOTES] congress={congress}")
    senate_votes = {}
    root = ElementTree.XML(session.get(SENATE_VOTES_URL).content)
    xmldict = XmlDictConfig(root)
    senate_vote_numbers = [v['vote_number'] for v in xmldict['votes']['vote']]
    progress = Progress(len(senate_vote_numbers))
    for senate_vote_number in senate_vote_numbers:
        print(next(progress), end=" ")
        url = f"{SENATE_VOTES_URL_BASE}{senate_vote_number}{SENATE_VOTES_URL_SUFFIX}"
        print(url)
        c = session.get(url).content
        root = ElementTree.XML(c)
        xmldict = XmlDictConfig(root)
        senate_votes[senate_vote_number] = xmldict

    house_votes = {}
    house_vote_numbers = depaginate_house_vote_numbers()
    progress = Progress(len(house_vote_numbers))
    for house_vote_number in house_vote_numbers:
        print(next(progress), end=" ")
        url = f"{HOUSE_VOTES_URL_BASE}{house_vote_number}"
        res = session.get(url)
        bs = BeautifulSoup(res.content, "html5lib")
        votes = bs.select("#member-votes > tr > td[data-label=vote]")
        anchors = bs.select("#member-votes > tr > td[data-label=member] > a")
        internal_codes = [anchor["href"][32:] for anchor in anchors]
        house_votes[house_vote_number] = list(zip(internal_codes, votes))

    return {
        "senate_votes": senate_votes,
        "house_votes": house_votes,
    }

def depaginate_house_vote_numbers():
    print("[DEPAGINATE_HOUSE_VOTE_NUMBERS]")
    house_vote_numbers = []
    page = 1
    while True:
        print(f"page={page}")
        res = session.get(HOUSE_VOTES_URL, params={"page": page})
        bs = BeautifulSoup(res.content, "html5lib")
        anchors = bs.select(".detail-button > a.btn-library")
        if len(anchors) == 0:
            return house_vote_numbers
        house_vote_numbers += [anchor["href"][7:] for anchor in anchors]
        page += 1

if __name__ == "__main__":
    with open(DATA_PATH, "w") as fp:
        data = []
        for congress in CONGRESSES:
            all = scrape_votes(congress)
            data += all
        print(f"Saving data to {DATA_PATH}...", end="")
        json.dump(data, fp)
    print("Done.")