CREATE TABLE bills (
	id INTEGER NOT NULL AUTO_INCREMENT,
	`type` ENUM('H.R.','S.B.','H.J.R.','S.J.R.','H.C.R.','S.C.R.','H.S.R.','S.S.R.'),
	`number` INTEGER,
	title TEXT,
	`description` TEXT,
	full_text_link TEXT,
	congress INTEGER,
	chamber ENUM('House','Senate'),
	subject_area ENUM('Agriculture and Food', 'Animals', 'Armed Forces and National Security', 'Arts, Culture, Religion', 'Civil Rights and Liberties, Minority Issues', 'Commerce', 'Congress', 'Crime and Law Enforcement', 'Economics and Public Finance', 'Education', 'Emergency Management', 'Energy', 'Environmental Protection', 'Families', 'Finance and Financial Sector', 'Foreign Trade and International Finance', 'Government Operations and Politics', 'Health', 'Housing and Community Development', 'Immigration', 'International Affairs', 'Labor and Employment', 'Law', 'Native Americans', 'Public Lands and Natural Resources', 'Science, Technology, Communications', 'Social Sciences and History', 'Social Welfare', 'Sports and Recreation', 'Taxation', 'Transportation and Public Works', 'Water Resources Development'),
	PRIMARY KEY (id)
);

CREATE TABLE congresspeople (
	id INTEGER NOT NULL AUTO_INCREMENT,
	first_name TEXT,
	last_name TEXT,
	middle_initial TEXT,
	party TEXT,
	party_initial TEXT,
	district TEXT, -- null for senators
	`state` TEXT,
	state_abbreviation TEXT,
	image_url TEXT,
	website_url TEXT,
	PRIMARY KEY (id)
);

CREATE TABLE committees (
	id INTEGER NOT NULL AUTO_INCREMENT,
	chair_id INTEGER NOT NULL,
	`name` TEXT,
	`type` ENUM('Standing','Special','Select','Other','Joint','Commission or Caucus'),
	chamber ENUM('House','Senate','Joint'),
	established_date TEXT, -- DATE?
	active BOOL,
	banner_image_url TEXT,
	PRIMARY KEY (id),
	FOREIGN KEY (chair_id) REFERENCES congresspeople(id),
	UNIQUE (`name`)
);

CREATE TABLE votes_yea (
	bill_id INTEGER NOT NULL,
	congressperson_id INTEGER NOT NULL,
	PRIMARY KEY (bill_id, congressperson_id),
	FOREIGN KEY (bill_id) REFERENCES bills(id),
	FOREIGN KEY (congressperson_id) REFERENCES congresspeople(id)
);

CREATE TABLE votes_nay (
	bill_id INTEGER NOT NULL,
	congressperson_id INTEGER NOT NULL,
	PRIMARY KEY (bill_id, congressperson_id),
	FOREIGN KEY (bill_id) REFERENCES bills(id),
	FOREIGN KEY (congressperson_id) REFERENCES congresspeople(id)
);

CREATE TABLE sponsorings (
	bill_id INTEGER NOT NULL,
	congressperson_id INTEGER NOT NULL,
	PRIMARY KEY (bill_id, congressperson_id),
	FOREIGN KEY (bill_id) REFERENCES bills(id),
	FOREIGN KEY (congressperson_id) REFERENCES congresspeople(id)
);

CREATE TABLE members (
	committee_id INTEGER NOT NULL,
	congressperson_id INTEGER NOT NULL,
	PRIMARY KEY (committee_id, congressperson_id),
	FOREIGN KEY (committee_id) REFERENCES committees(id),
	FOREIGN KEY (congressperson_id) REFERENCES congresspeople(id)
);

CREATE TABLE actions_by_committee (
	committee_id INTEGER NOT NULL,
	bill_id INTEGER NOT NULL,
	PRIMARY KEY (committee_id, bill_id),
	FOREIGN KEY (committee_id) REFERENCES committees(id),
	FOREIGN KEY (bill_id) REFERENCES bills(id)
);

CREATE TABLE actions_on_bill (
	bill_id INTEGER NOT NULL,
	`action` TEXT,
	`date` TEXT, -- DATE?
	sequence_number INTEGER NOT NULL, -- the date field has a granularity of one day, and some actions take place on the same day, so this field is needed for sorting
	PRIMARY KEY (bill_id, sequence_number),
	FOREIGN KEY (bill_id) REFERENCES bills(id)
);
