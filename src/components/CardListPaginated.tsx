import { Pagination } from "react-bulma-components";
import { Suspense, useState } from "react";
import CardListAPI from "./CardListApi";
import { CardListSkeleton } from "./CardListSkeleton";
import { useSearchParameters } from "../urlParamSync"

interface Props<T> {
  urlFn(page: number): string;
  apiToCardContentsFn(apiOutput: T): JSX.Element;
}

export default function CardListPaginated<T>(props: Props<T>) {
  const { setSearchParam, getSearchParam } = useSearchParameters()

  const [page, setPage] = useState(+(getSearchParam("page") || "1"));
  function setPageAndScroll(page : number) {
    setPage(page)
    setSearchParam('page', page)
    window.scrollTo(0, 0)
  }
  const [numPages, setNumPages] = useState(1);

  return (
    <>
      <Suspense fallback={<CardListSkeleton count={12} />}>
        <CardListAPI<T>
          url={props.urlFn(page)}
          apiToCardContentsFn={props.apiToCardContentsFn}
          setNumPages={setNumPages}
        />
      </Suspense>
      <div className="is-flex-grow-1"></div>
      <Pagination
        current={page}
        onChange={setPageAndScroll}
        showFirstLast
        total={numPages}
        delta={2}
      />
    </>
  );
}
