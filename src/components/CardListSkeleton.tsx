import CardList from "./CardList";

interface Props {
  count: number;
}

export function CardListSkeleton(props: Props) {
  const skeletonCardFn = () => (
    <>
      <div>
      <div className="card__header">
        <div>

        </div>
        <h3 className="card__header header__title mb-4" >
          <div className="skeleton skeleton-text"></div>
        </h3>
      </div>

      <div className="card__body mb-5">
        <div className="card__body body__text" >
          <div className="skeleton skeleton-text skeleton-text__body"></div>
          <div className="skeleton skeleton-text skeleton-text__body"></div>
          <div className="skeleton skeleton-text skeleton-text__body"></div>
          <div className="skeleton skeleton-text skeleton-text__body"></div>
          <div className="skeleton skeleton-text skeleton-text__body"></div>
          <div className="skeleton skeleton-text skeleton-text__body"></div>
        </div>
      </div>

      <div className="card__footer" >
        <div className="skeleton skeleton-text skeleton-footer"></div>
      </div>
      </div>
    </>
  );
  return (
    <CardList contents={Array(props.count).fill(null).map(skeletonCardFn)} />
  );
}
