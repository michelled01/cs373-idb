import { useGetData } from "../api";
import CardList from "./CardList";
import { Paginated } from "../types";
import { useEffect } from "react";

interface Props<T> {
  url: string;
  apiToCardContentsFn(apiOutput: T): JSX.Element;
  setNumPages: React.Dispatch<React.SetStateAction<number>>;
}

export default function CardListAPI<T>(props: Props<T>) {
  const data = useGetData<Paginated<T>>(props.url);
  useEffect(() => {
    if (data) {
      props.setNumPages(
        Math.ceil(data.total_items / data.items_per_page)
      );
    }
  });

  if (data) {
    return (
      <>
        <CardList contents={data.items.map(props.apiToCardContentsFn)} />
      </>
    );
  } else {
    return <></>;
  }
}
