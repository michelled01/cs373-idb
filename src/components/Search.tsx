import { useState, Suspense } from "react"
import { useGetData } from "../api"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
import { faGear } from '@fortawesome/free-solid-svg-icons'
import { useSearchParameters } from "../urlParamSync"

interface Props {
  updateURLParams?: React.Dispatch<React.SetStateAction<string>>;
  updateSearchTerms?: React.Dispatch<React.SetStateAction<string[]>>;
  modelEndpoint?: '/bill_properties' | '/congressperson_properties' | '/committee_properties' | '/search';
}

export function Search(props: Props) {
  // const billProperties = useGetData<any>('/bill_properties')
  // const congresspersonProperties = useGetData<any>('/congressperson_properties')
  // const committeeProperties = useGetData<any>('/committee_properties')
  let model = useGetData<any>(props.modelEndpoint?.toString() || "")
  // let universal = [billProperties, congresspersonProperties, committeeProperties]

  const { setSearchParam, getSearchParam } = useSearchParameters()

  const [sortAscending, setSortAscending] = useState(getSearchParam("sort_ascending") !== 'true')

  const [searchConjunction] = useState(getSearchParam("search_conjunction"))

  const [isActive, setIsActive] = useState(false)
  const [ops, setOps] = useState<Record<string, string>>({})

  const handleSubmit = (e: any) => {
    e.preventDefault()

    const formElement = document.getElementById("searchForm")
    if (formElement) {
      const formData = new FormData(formElement as HTMLFormElement)
      const newFormData = new FormData()

      const search = formData.get('search')
      if (search) {
        setSearchParam("search", search)
        if (props.updateSearchTerms) {
          console.log(search.toString().trimStart().trimEnd().split(/\s+/))
          props.updateSearchTerms(search.toString().trimStart().trimEnd().split(/\s+/))
        }
        newFormData.append('search', search)
      } else {
        if (props.updateSearchTerms) {
          props.updateSearchTerms([])
        }
      }
      if (searchConjunction) {
        setSearchParam("search_conjunction", searchConjunction)
        newFormData.append('search_conjunction', searchConjunction)
      }

      const sortBy = formData.get('sort_by')
      if (sortBy) {
        setSearchParam("sort_by", sortBy)
        newFormData.append('sort_by', sortBy.toString())
      }
      setSearchParam("sort_ascending", sortAscending)
      newFormData.append('sort_ascending', sortAscending.toString())

      const filterBy = formData.get('filter_by')
      const filterOp = formData.get('op')
      const filterValue = formData.get('filter_value')

      if (filterBy && filterValue) {
        setSearchParam('filter_by', filterBy)
        setSearchParam('op', filterOp)
        setSearchParam('filter_value', filterValue)
        setSearchParam(filterBy.toString(), `${filterOp}_${filterValue}`)
        newFormData.append(filterBy.toString(), `${filterOp}_${filterValue}`)
      }

      const url = (new URLSearchParams(newFormData as any)).toString()
      console.log(url)
      if (props.updateURLParams) {
        props.updateURLParams(url) /* magic */
      }
      setIsActive(false)
    }
  }

  const toggleSortAscending = (e: any) => {
    setSortAscending(!sortAscending)
  }

  // const toggleSearchConjunction = (e: any) => {
  //   setSearchConjunction((searchConjunction === "and") ? "or" : "and")
  // }

  const populateOps = (e: any, model: any, modelEndpoint: any, universal: any) => {
    const formElement = document.getElementById("searchForm")
    if (formElement) {
      const formData = new FormData(formElement as HTMLFormElement)
      const filterBy = formData.get('filter_by')

      if (modelEndpoint === '/search') {
        for (let i = 0; i < 3; i++) {
          universal[i].forEach((b: any) => {
            if (b.api_name === filterBy) {
              let newOps = { 'eq': '=', 'ne': '!=' } as Record<string, string>
              if (b.inequality_filterable) {
                newOps = { ...newOps, 'le': '<=', 'ge': '>=', 'lt': '<', 'gt': '>' }
              }
              setOps(ops => newOps)
              return
            }
          })
        }
      }
      else {
        model.forEach((b: any) => {
          if (b.api_name === filterBy) {
            let newOps = { 'eq': '=', 'ne': '!=' } as Record<string, string>
            if (b.inequality_filterable) {
              newOps = { ...newOps, 'le': '<=', 'ge': '>=', 'lt': '<', 'gt': '>' }
            }
            setOps(ops => newOps)
            return
          }
        })
      }
    }
  }

  return (
    <>
      <form id="searchForm" className="mb-3" onSubmit={e => e.preventDefault()}>
        <div className="field has-addons">
          <div className={isActive ? "dropdown is-active control is-expanded" : "dropdown control is-expanded"}>
            <div className="control is-expanded">
              <input
                className="input"
                type="text"
                name="search"
                // TODO make the placeholder text a prop
                placeholder="Find something specific"
              />
            </div>
            {/* <div className="control">
              <button className="button" onClick={toggleSearchConjunction}>
                Match {(searchConjunction === 'and' ? "all" : " any")}
              </button>
            </div> */}
            <div className="control">
              {/* TODO make this link to a bills page with url params passed through that do searching? */}
              <button className="button is-primary" onClick={handleSubmit}>
                <span className="icon ">
                  <FontAwesomeIcon icon={faMagnifyingGlass} />
                </span>
              </button>
            </div>
            <Suspense fallback="">
              <div className="control">
                <button className="button" onClick={() => setIsActive(!isActive)}>
                  <span className="icon ">
                    <FontAwesomeIcon icon={faGear} />
                  </span>
                </button>
              </div>
              <AdvancedSearch modelEndpoint={props.modelEndpoint!} model={model} isActive={isActive} sortAscending={sortAscending} searchConjunction={searchConjunction!} toggleSortAscending={toggleSortAscending} handleSubmit={handleSubmit} populateOps={populateOps} ops={ops} />
            </Suspense>
          </div>
        </div>
      </form>
    </>
  );
}

interface AdvancedSearchProps {
  modelEndpoint: '/bill_properties' | '/congressperson_properties' | '/committee_properties' | '/search',
  model: any,
  isActive: boolean,
  sortAscending: boolean,
  searchConjunction: string,
  toggleSortAscending: React.MouseEventHandler,
  handleSubmit: React.MouseEventHandler,
  populateOps: Function,
  ops: Record<string, string>
}

function AdvancedSearch(props: AdvancedSearchProps) {

  const { getSearchParam } = useSearchParameters()
  const isDropdownable = false

  const billProperties = useGetData<any>('/bill_properties')
  const congresspersonProperties = useGetData<any>('/congressperson_properties')
  const committeeProperties = useGetData<any>('/committee_properties')
  let universal = [billProperties, congresspersonProperties, committeeProperties]

  if (props.model) {

    // const sortable_fields = new Set<Record<string,string>>([]) 
    // if (universal.length !== 0) {
    //   Object.values(universal)
    //     .map((m) => (
    //       Object.keys(m)
    //         .filter(item => m[item].sortable)
    //         .map((item, i) => (
    //           let var = {m[item].display_name : m[item].api_name} as Record<string,string>
    //           sortable_fields.add(var) 
    //         ))
    //     ))
    // }
    // console.log(sortable_fields)

    return (
      <div className={"search-filters box w-100 mt-3 p-5 pb-6" + (props.isActive ? "" : " is-hidden")}>
        <h3 className="title is-4">Advanced Search</h3>
        <div className="field is-grouped">
          <div className="field has-addons mr-4">
            <button className="button is-static">
              Sort by
            </button>
            <p className="control">
              <span className="select">
                <select name="sort_by" defaultValue={getSearchParam('sort_by') || ""}>
                  <option value="">None</option>
                  {(props.modelEndpoint.toString() === '/search') ?
                    Object.values(universal)
                      .map((m) => (
                        Object.keys(m)
                          .filter(item => m[item].sortable)
                          .map((item, i) => (
                            <option key={i} value={m[item].api_name}>
                              {m[item].display_name}
                            </option>
                          ))
                      ))
                    :
                    Object.keys(props.model)
                      .filter(item => props.model[item].sortable)
                      .map((item, i) => (
                        <option key={i} value={props.model[item].api_name}>
                          {props.model[item].display_name}
                        </option>
                      ))}
                </select>
              </span>
            </p>
          </div>
          <div className="field has-addons">
            <button
              name="sort_ascending"
              value={props.sortAscending.toString()}
              onClick={props.toggleSortAscending}
              className={"button is-primary is-light"}
            >
              Sort by {(props.sortAscending ? "Ascending" : " Descending")}
            </button>
          </div>
        </div>
        <div className="field is-grouped is-grouped-multiline mb-4">
          <div className="field has-addons mr-3">
            <button className="button is-static">
              Filter by
            </button>
            <p className="control">
              <span className="select">
                <select name="filter_by" onChange={(e) => props.populateOps(e, props.model, props.modelEndpoint, universal)} defaultValue={getSearchParam('filter_by') || ""}>
                  <option value="">None</option>
                  {(props.modelEndpoint.toString() === '/search') ?
                    Object.values(universal)
                      .map((m) => (
                        Object.keys(m)
                          .filter(item => m[item].sortable)
                          .map((item, i) => (
                            <option key={i} value={m[item].api_name}>
                              {m[item].display_name}
                            </option>
                          ))
                      ))
                    :
                    Object.keys(props.model)
                      .filter(item => props.model[item].sortable)
                      .map((item, i) => (
                        <option key={i} value={props.model[item].api_name}>
                          {props.model[item].display_name}
                        </option>
                      ))}
                </select>
              </span>
            </p>
          </div>
          <div className="field has-addons">
            <button className="button is-static">
              Type
            </button>
            <p className="control">
              <span className="select">
                <select name="op" defaultValue={getSearchParam('op') || ""}>
                  {Object.entries(props.ops).map(([key, value], i) => (
                    <option key={i} value={key}>
                      {value}
                    </option>
                  ))}
                </select>
              </span>
            </p>
            {/* STRETCH GOAL: make this a dropdown based on user input. will require more work from the API endpoint */}

            {isDropdownable ?
              <p className="control is-expended">
              </p> :
              <div className="control is-expanded">
                <input
                  className="input"
                  type="text"
                  placeholder="Input a num, string, or date"
                  name="filter_value"
                  defaultValue={getSearchParam('filter_value') || ""}
                />
              </div>
            }
          </div>
        </div>
        <button className="button is-primary" onClick={props.handleSubmit}>
          <span>Submit</span>
        </button>
      </div>
    )
  } else {
    return <></>
  }
}
