

interface Props {
    contents: JSX.Element[]
}

export default function CardList(props : Props) {
  return (
    <div className="columns is-multiline">
      {props.contents.map((x, i) => (
        <div className="column is-half is-one-third-fullhd is-flex" key={i}>
          <div className="card is-flex is-flex-grow-1">
            <div className="card-content is-flex is-flex-direction-column max-width-full w-100">
              {x}
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}
