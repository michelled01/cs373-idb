interface Props {
  data: { title: string; description: JSX.Element }[];
}

export default function Timeline(props: Props) {
  return (
    <div className="timeline">
      {props.data.map((x, i) => (
        <div className="timeline-item" key={i}>
          <div className="timeline-marker"></div>
          <div className="timeline-content">
            <p className="heading">{x.title}</p>
            {x.description}
          </div>
        </div>
      ))}
    </div>
  );
}
