import { useState } from "react";
import { NavLink } from "react-router-dom";

export default function Navbar() {
  const [isActive, setIsActive] = useState(false);

  return (
    <nav
      className="navbar is-primary"
      role="navigation"
      aria-label="main navigation"
    >
      <div className="container">
        <div className="navbar-brand m-0">
          <a className="navbar-item" href="../favicon.ico">
          <img src="/favicon.ico" alt='bill-on-computer'></img>
          </a>
          <NavLink className="navbar-item title is-5 m-0" to="/">
            LegisLore
          </NavLink>

          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a
            role="button"
            className={isActive ? "navbar-burger is-active" : "navbar-burger"}
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
            onClick={() => setIsActive(!isActive)}
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>

        <div
          id="navbarBasicExample"
          className={isActive ? "navbar-menu is-active" : "navbar-menu"}
        >
          <div className="navbar-start">
            {["Bills", "Congresspeople", "Committees", "About LegisLore"].map((x, i) => (
              <NavLink
                onClick={() => setIsActive(false)}
                key={i}
                to={"/" + x.replace(/ /g, "_").toLowerCase()}
                className={({ isActive }) =>
                  isActive ? "navbar-item is-active" : "navbar-item"
                }
              >
                {x}
              </NavLink>
            ))}
          </div>
        </div>
      </div>
    </nav>
  );
}
