export default function Footer() {
  return (
    <footer className="footer">
      <div className="content has-text-centered">
        <p>
          <strong>LegisLore</strong> by Isaac Hammer, Patrick Cammarata, Michelle Ding, Tim Jones, and Jonathan Chacko.
        </p>
      </div>
    </footer>
  );
}
