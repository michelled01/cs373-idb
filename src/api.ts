import { useState, useEffect } from "react";

const FALLBACK_URL = "/data"
const API_URL = process.env.NODE_ENV === 'production' ? "https://legislore.uc.r.appspot.com/api" : "http://localhost:5000/api"
const TRY_TO_USE_API_ON_PRODUCTION = true

// Try to make a request to the API. If it's up, let's use that! 
// If it's not, fall back to static json files.
const getURLMaker = new Promise<Function>((resolve) => {
    if (process.env.NODE_ENV === 'development' || TRY_TO_USE_API_ON_PRODUCTION) {
    fetch(API_URL).then(x => {
        if (x.status === 200) {
            // We have a server running! Let's use it!
            console.log("Using API!")
            resolve((path : string) => API_URL + path)
        }
    }).catch(error => {
        console.log(error)
        console.log("Error getting API data, falling back to static files...")
        resolve((path : string) => FALLBACK_URL + path.split("?")[0] + ".json")
    })
    } else {
        resolve((path : string) => FALLBACK_URL + path.split("?")[0] + ".json")
    }
})

const delay = (ms: number) => new Promise(res => setTimeout(res, ms));

// Magic from https://deadsimplechat.com/blog/react-suspense/
const promiseWrapper = <T>(promise: Promise<T>): () => T => {
    let status = "pending";
    let result: T;

    const s = promise.then(
        (value) => {
            status = "success";
            result = value;
        },
        (error) => {
            status = "error";
            result = error;
        }
    );

    return () => {
        switch (status) {
            case "pending":
                throw s;
            case "success":
                return result;
            case "error":
                throw result;
            default:
                throw new Error("Unknown status");
        }
    };
};

export function useGetData<T>(path: string) {
    const [resource, setResource] = useState<T>();

    useEffect(() => {
        const getData = async () => {
            const promise = getURLMaker
                .then(makeURL => fetch(makeURL(path))) // TODO remove .json when actual server gets here
                .then(async (response) => {
                    if (process.env.NODE_ENV === 'development') {
                        await delay(500)  // TODO remove this, here to test frontend while waiting for response
                    }
                    return response
                })
                .then((response) => response.json());
            setResource(promiseWrapper(promise));
        };

        getData();
    }, [path]);

    return resource;
}

