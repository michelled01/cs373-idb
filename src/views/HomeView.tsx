import { Link } from "react-router-dom";
import CardList from "../components/CardList";
import { useGetData } from "../api";
import { Search } from "../components/Search";
import { Bill, Committee, Congressperson, Paginated } from "../types";
import { useState, Suspense } from "react";
import { CardListSkeleton } from "../components/CardListSkeleton";
import { attemptToFormatDescription, highlightText } from "./BillsView";
import { useSearchParameters } from "../urlParamSync"


function BillCardList(props: {
  urlParams: any;
  searchTerms: any
}) {
  const bills = useGetData<Paginated<Bill>>(`/bills?page=1&items_per_page=12&${props.urlParams}`)?.items || [];

  const billsCardContents = bills
    // .sort(() => 0.5 - Math.random())
    .slice(0, 3)
    .map((x) => (
      <>
        <div className="is-flex is-flex-direction-column is-flex-grow-1">
          <p className="title is-4 mb-3 is-truncated"
          dangerouslySetInnerHTML={{ __html: highlightText(x.name.type+' '+x.name.number+': '+x.name.title, props.searchTerms) }}>
          </p>
          <p className="block is-6 is-truncated"
           dangerouslySetInnerHTML={{ __html: highlightText(attemptToFormatDescription(x.description), props.searchTerms) }}>
          </p>
          {/* The line below does spacing, its important */}
          <div className="is-flex-grow-1"></div>
          <div className="content">
            <Link
              to={"/bills/" + x.id.toString()}
              className="button is-primary is-light"
            >
              Read more
            </Link>
          </div>
        </div>
      </>
    ));
  return <CardList contents={billsCardContents}></CardList>;
}

function CongresspeopleCardList(props: {
  urlParams: any;
  searchTerms: any
}) {
  const congresspeople =
    useGetData<Paginated<Congressperson>>(`/congresspeople?page=1&items_per_page=16&${props.urlParams}`)?.items || [];
  const congresspeopleCardContents = congresspeople
    // .sort(() => 0.5 - Math.random())
    .slice(0, 3)
    .map((x) => (
      <>
        <p className="title is-4 mb-3"
        dangerouslySetInnerHTML={{ __html: highlightText(x.name.first+' '+x.name.last+' ('+x.party_initial+'-'+x.state_abbreviation+')', props.searchTerms)}}>
        </p>
        <p className="block is-6 is-flex-grow-1"
        dangerouslySetInnerHTML={{ __html: highlightText(x.chamber+', sponsored '+x.relations.total_sponsored_bills+' bill'+(x.relations.total_sponsored_bills !== 1 ? "s" : ""), props.searchTerms)}}>
        </p>
        <div className="content">
          <Link
            to={"/congresspeople/" + x.id.toString()}
            className="button is-primary is-light"
          >
            Read more
          </Link>
        </div>
      </>
    ));
  return <CardList contents={congresspeopleCardContents} />;
}

function CommitteesCardList(props: {
  urlParams: any;
  searchTerms: any
}) {
  const committees =
    useGetData<Paginated<Committee>>(`/committees?page=1&items_per_page=12&${props.urlParams}`)?.items || [];

  const committeesCardContents = committees
    // .sort(() => 0.5 - Math.random())
    .slice(0, 3)
    .map((x) => (
      <>
        <p className="title is-4 mb-3"
        dangerouslySetInnerHTML={{ __html: highlightText(x.name, props.searchTerms)}}>
        </p>
        <p className="block is-6 is-flex-grow-1"
        dangerouslySetInnerHTML={{ __html: highlightText(x.relations.total_members+' members, '+x.relations.total_bills+' bills', props.searchTerms)}}>
        </p>
        <div className="content">
          <Link
            to={"/committees/" + x.id.toString()}
            className="button is-primary is-light"
          >
            Read more
          </Link>
        </div>
      </>
    ));
  return <CardList contents={committeesCardContents}></CardList>;
}

export default function HomeView() {
  const { getDefaultParams } = useSearchParameters()
  const [urlParams, updateURLParams] = useState(getDefaultParams())
  const [searchTerms, updateSearchTerms] = useState<string[]>([])

  return (
    <>
      <h1 className="title">Welcome to LegisLore</h1>
      <Search updateURLParams={updateURLParams} updateSearchTerms={updateSearchTerms} modelEndpoint='/search' />
      <div className="block">
        <h2 className="is-size-3 has-text-weight-bold mb-2">Bills</h2>
        {/* TODO make this a carousel? */}
        <Suspense fallback={<CardListSkeleton count={3} />}>
          <BillCardList urlParams={urlParams} searchTerms={searchTerms} />
        </Suspense>
        <Link to="/bills" className="button is-primary">
          See More Bills
        </Link>
      </div>

      <div className="block">
        <h2 className="is-size-3 has-text-weight-bold mb-2">Congresspeople</h2>
        {/* TODO make this a carousel? */}
        <Suspense fallback={<CardListSkeleton count={3} />}>
          <CongresspeopleCardList urlParams={urlParams} searchTerms={searchTerms} />
        </Suspense>
        <Link to="/congresspeople" className="button is-primary">
          See More Congresspeople
        </Link>
      </div>

      <div className="block">
        <h2 className="is-size-3 has-text-weight-bold mb-2">Committees</h2>
        {/* TODO make this a carousel? */}
        <Suspense fallback={<CardListSkeleton count={3} />}>
          <CommitteesCardList urlParams={urlParams} searchTerms={searchTerms} />
        </Suspense>
        <Link to="/committees" className="button is-primary">
          See More Committees
        </Link>
      </div>
    </>
  );
}
