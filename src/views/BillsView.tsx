import { Link } from "react-router-dom";
import { Search } from "../components/Search";
import CardListPaginated from "../components/CardListPaginated";
import { Bill } from "../types";
import { useState } from "react";
import { useSearchParameters } from "../urlParamSync"

export function attemptToFormatDescription(description: string | null) {
  if (!description) {
    return "No description found.";
  }

  const split = description.split("<p>");

  if (split.length > 1) {
    if (
      (split[1].includes("<b>") || split[1].includes("<strong>")) &&
      split.length > 2
    ) {
      // split[1] is a title, get the next one
      return split[2].replace("</p>", "").replace(/<\/?[^>]+(>|$)/g, "");
    } else {
      return split[1].replace("</p>", "").replace(/<\/?[^>]+(>|$)/g, "");
    }
  }

  return description[0].replace(/<\/?[^>]+(>|$)/g, ""); // strip html from string
}

// TODO: make tokens reactive with useEffect?
export function highlightText(formattedText: string | null, tokens: string[]) {
  // console.log("toekns:" , tokens)
  if (!formattedText)
    return ""
  if (tokens.length === 0) {
    return formattedText
  }
  // console.log("somehow got iere")
  for (let i = 0; i < tokens.length; i++) {
    const regex = new RegExp(tokens[i], "gi");
    formattedText = formattedText.replaceAll(regex, function(match) {
      return `<mark>${match}</mark>`
    })
  }
  return formattedText
}

export default function BillsView() {

  const { getDefaultParams } = useSearchParameters()
  const [urlParams, updateURLParams] = useState(getDefaultParams())
  const [searchTerms, updateSearchTerms] = useState<string[]>([])

  const cardContentsFn = (bill: Bill) => (
    <>
      <p className="title is-4 mb-3 is-truncated"
      dangerouslySetInnerHTML={{ __html: highlightText(bill.name.type+' '+bill.name.number+': '+bill.name.title, searchTerms) }}>
      </p>

      <p className="block is-6 is-truncated"
        dangerouslySetInnerHTML={{ __html: highlightText(attemptToFormatDescription(bill.description), searchTerms) }}
      >
      </p>

      <p className="is-flex-grow-1">
        {/* This is here for spacing, don't remove it */}
      </p>

      <div className="content">
        {bill.relations.total_votes_nay > 0 &&
          bill.relations.total_votes_yea > 0 ? (
          <div className="buttons has-addons no-pointer">
            <button className="button is-success is-light">
              {bill.relations.total_votes_yea} Yea
            </button>
            <button className="button is-danger is-light">
              {bill.relations.total_votes_nay} Nay
            </button>
          </div>
        ) : (
          ""
        )}

        <div className="tags">
          {bill.subject_area ? (
            <span className="tag is-info long-tag">{bill.subject_area}</span>
          ) : (
            ""
          )}
          <span className="tag truncate-tag">{bill.actions[0].action}</span>
        </div>

        <div className="buttons">
          <Link to={bill.id.toString()} className="button is-primary">
            Open Bill
          </Link>
          {bill.full_text_link ? (
            <a
              href={bill.full_text_link}
              target="_blank"
              className="button is-link mt-0 is-light"
              rel="noreferrer"
            >
              Read Full Text
            </a>
          ) : (
            ""
          )}
        </div>
      </div>
    </>
  );

  return (
    <>
      <h1 className="title">Bills</h1>
      <Search updateURLParams={updateURLParams} updateSearchTerms={updateSearchTerms} modelEndpoint='/bill_properties' />
      <CardListPaginated<Bill>
        urlFn={(page: number) => `/bills?page=${page}&items_per_page=12&${urlParams}`} /* sends request to backend api */
        apiToCardContentsFn={cardContentsFn}
      />
    </>
  );
}
