export default function NotFound() {
  return (
    <>
      <h1 className="title">Page Not Found</h1>
      <p>Sorry, we weren't able to find that page.</p>
    </>
  );
}
