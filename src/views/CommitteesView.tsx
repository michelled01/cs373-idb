import { Link } from "react-router-dom";
import { Search } from "../components/Search";
import { Committee } from "../types";
import CardListPaginated from "../components/CardListPaginated";
import { useState } from "react";
import { highlightText } from "./BillsView";

export default function CommitteesView() {

  const [urlParams, updateURLParams] = useState("")
  const [searchTerms, updateSearchTerms] = useState<string[]>([])

  const cardContentsFn = (x: Committee) => (
    <>
      <p className="title is-4 mb-2"
                  dangerouslySetInnerHTML={{ __html: highlightText(x.name, searchTerms) }}>
                    </p>
      <p className="">
        Established{" "}
        {new Date(x.established_date).toLocaleDateString("en-us", {
          year: "numeric",
          month: "long",
          day: "numeric",
        })}
      </p>
      <p className="block is-flex-grow-1"
                  dangerouslySetInnerHTML={{ __html: highlightText(x.relations.total_bills + ' bills, ' + x.relations.total_members + ' members', searchTerms) }}>
      </p>
      <div className="content">
        <Link to={x.id.toString()} className="button is-primary">
          Open Committee
        </Link>
      </div>
    </>
  );

  return (
    <>
      <h1 className="title">Committees</h1>
      <Search updateURLParams={updateURLParams} updateSearchTerms={updateSearchTerms} modelEndpoint='/committee_properties'/>
      <CardListPaginated<Committee>
        urlFn={(page: number) => `/committees?page=${page}&items_per_page=12&${urlParams}`}
        apiToCardContentsFn={cardContentsFn}
      />
    </>
  );
}
