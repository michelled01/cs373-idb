import { useGetData } from "../api";
import CardList from "../components/CardList";
import { GitlabStats } from "../types";
import { Link } from "react-router-dom";

interface Contributor {
  name: string;
  responsibilities?: string;
  img_url?: string;
  bio: string;
}

interface Tool {
  name: string;
  link: string;
}

export default function AboutView() {
  const gitlabStats = useGetData<GitlabStats>("/gitlab_stats");

  const contributors: Contributor[] = [
    {
      name: "Michelle Ding",
      responsibilities: "Front-end (UI design, Linking pages to API)",
      bio: "I am a third-year CS and Math Major at UT Austin. My native hometown is Houston, TX. Outside of school, I enjoy running, hiking, ice skating, and making art.",
      img_url: "/assets/contributors/michelle.png",
    },
    {
      name: "Patrick Cammarata",
      responsibilities: "Back-end (API Design/Collecting instances for models)",
      bio: "I'm a fourth-year CS major at UT Austin. I grew up and spent most of life in a suburb near Dallas, TX. In my free time, I like cooking, swimming, and hiking.",
      img_url: "/assets/contributors/patrick.png",
    },
    {
      name: "Isaac Hammer",
      responsibilities:
        "Back-end (API design+impl, database design+impl, data collection)",
      bio: "I'm a fourth-year CS major at UT Austin. I've lived in Austin all my life. In my free time, I like hiking (going to Olympic NP this August!), biking, and not swimming.",
      img_url: "/assets/contributors/isaac.png",
    },
    {
      name: "Tim Jones",
      responsibilities:
        "GCP (Firebase, App Engine) + Namecheap Setup + Gitlab Mechanical Turk",
      bio: "I am a 31st year CS Major at UT Austin. I am originally from Clear Lake City, Texas and have been a resident of Austin since 2006. I enjoy hiking and thinking about dogs I might adopt but haven't yet.",
      img_url: "/assets/contributors/tim.png",
    },
    {
      name: "Jonathan Chacko",
      responsibilities: "Front-end (UI design, Linking pages to API)",
      bio: "I'm currently a senior CS major at UT Austin. I'm currently working as a research assistant for the Cenik Lab, building browser-based tools to visualize ribosome profiling experiment data. I'm originally from Houston, TX and in my free time I love playing piano and hiking.",
      img_url: "/assets/contributors/jonathan.jpeg",
    },
    {
      name: "Mr. Bill",
      responsibilities: "Mascot",
      bio: "My name is Mr. Bill, and I am currently sitting on Capital Hill. In fact, I am waiting outside of Congress right now to be interviewed by a committee, a select group of congressmen who get to make the Big Future Decisions about my life. Learn more about me through this website. ",
      img_url: "/assets/contributors/bill.png",
    }
  ];

  const tools: Tool[] = [
    {
      name: "Postman",
      link: "https://documenter.getpostman.com/view/16329964/2s93z6djFD",
    },
    {
      name: "Firebase Hosting",
      link: "https://firebase.google.com/docs/hosting",
    },
    {
      name: "GCP App Engine",
      link: "https://cloud.google.com/appengine",
    },
    {
      name: "React",
      link: "https://react.dev/",
    },
    {
      name: "TypeScript",
      link: "https://www.typescriptlang.org/",
    },
    {
      name: "Bulma",
      link: "https://bulma.io/",
    },
    {
      name: "Sass",
      link: "https://sass-lang.com/",
    },
    {
      name: "Flask",
      link: "https://flask.palletsprojects.com/en/2.3.x/",
    },
    {
      name: "SQLAlchemy",
      link: "https://www.sqlalchemy.org/",
    },
    {
      name: "Gunicorn",
      link: "https://gunicorn.org/",
    },
  ];

  const contributorCardContents = contributors.map((x, i) => (
    <div
      className="is-flex is-flex-direction-column is-flex-grow-1 is-align-items-baseline"
      key={i}
    >
      <img src={x.img_url} alt={x.name} className="about-page-img mb-3" />

      <p className="title is-4 mb-3">{x.name}</p>
      <span className="tag is-info long-tag mb-3">{x.responsibilities}</span>
      {x.name === 'Mr. Bill' ? 
      <ul className="mb-3">
      <li>
        Moral Support: 100+
      </li>
        </ul>
      :
        <ul className="mb-3">
          <li>
            Commits:{" "}
            {gitlabStats?.commits ? gitlabStats.commits[x.name] : "empty"}
          </li>
          <li>
            Issues authored:{" "}
            {gitlabStats?.issues ? gitlabStats.issues[x.name] : "empty"}
          </li>
          <li>
            Unit tests:{" "}
            {gitlabStats?.unit_tests ? gitlabStats.unit_tests[x.name] : "empty"}
          </li>
        </ul>
      }
      <p className="block is-6 is-flex-grow-1">{x.bio}</p>
    </div>
  ));

  const toolContents = tools.map((x, i) => (
    <Link to={x.link} target="_blank" key={i}>
      <span className="tag">{x.name}</span>
    </Link>
  ));

  return (
    <>
      <div className="block">
        <h1 className="title">About LegisLore</h1>
        <p className="is-size-5">
          LegisLore aims to provide at-a-glance insights into the bills in the
          national congress, the congresspeople who voted (Y/N/other) on them,
          and the committees that introduced the bills.
        </p>
      </div>

      {gitlabStats ? (
        <>
          <nav className="level mb-6">
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Commits</p>
                <p className="title">{gitlabStats?.total_commits}</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Issues</p>
                <p className="title">{gitlabStats?.total_issues}</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Unit Tests</p>
                <p className="title">{gitlabStats?.total_unit_tests}</p>
              </div>
            </div>
          </nav>
        </>
      ) : (
        ""
      )}
      <div className="block">
        <h1 className="title is-4 mb-4">Contributors</h1>
        <CardList contents={contributorCardContents} />
      </div>
      {gitlabStats ? (
        <>
          <div className="block">
            <h1 className="title is-4 mb-4">Important Links</h1>
            <div className="buttons">
              <a
                target="_blank"
                rel="noreferrer"
                className="button"
                href={gitlabStats.gitlab_issue_tracker_url}
              >
                Open Issue Tracker
              </a>
              <a
                target="_blank"
                rel="noreferrer"
                className="button"
                href={gitlabStats.gitlab_repo_url}
              >
                Open Repo
              </a>
              <a
                target="_blank"
                rel="noreferrer"
                className="button"
                href={gitlabStats.gitlab_wiki_url}
              >
                Open Wiki
              </a>
              <a
                target="_blank"
                rel="noreferrer"
                className="button"
                href={gitlabStats.postman_url}
              >
                Open Postman
              </a>
              <a
                target="_blank"
                rel="noreferrer"
                className="button"
                href="https://speakerdeck.com/isaachammer/legislore"
              >
                Presentation
              </a>
              
            </div>
          </div>

          <div className="block">
            <h1 className="title is-4 mb-4">Data Sources</h1>
            <p className="mb-2">
              We used{" "}
              <Link
                to="https://python-gitlab.readthedocs.io/en/stable/"
                target="_blank"
              >
                python-gitlab
              </Link>{" "}
              to fetch the live repo stats above. This runs on the backend,
              forwarding the data through the LegisLore API so as to not expose
              our GitLab API private token to the frontend.
            </p>
            <div className="buttons">
              <a
                target="_blank"
                rel="noreferrer"
                className="button"
                href={"https://api.congress.gov/"}
              >
                Congress API
              </a>
              <a
                target="_blank"
                rel="noreferrer"
                className="button"
                href={"https://api.govinfo.gov/docs/"}
              >
                GovInfo API
              </a>
              <a
                target="_blank"
                rel="noreferrer"
                className="button"
                href={"https://www.mediawiki.org/wiki/API:Main_page"}
              >
                Wikipedia API
              </a>
              <a
                target="_blank"
                rel="noreferrer"
                className="button"
                href={"https://clerk.house.gov/"}
              >
                Clerk House API
              </a>
            </div>
          </div>
        </>
      ) : (
        ""
      )}

      <div className="block">
        <div className="title is-size-4 mb-2">Tools Used</div>
        <div className="tags">{toolContents} </div>
      </div>
    </>
  );
}
