import { useParams } from "react-router";
import Timeline from "../components/Timeline";
import { useGetData } from "../api";
import { Bill } from "../types";
import { Link } from "react-router-dom";

export default function BillView() {
  const { id } = useParams();
  const bill = useGetData<Bill>("/bill/" + id);

  // function transformBillDescription(description: string) {
  //   return description
  //     .split("<p>")
  //     .slice(2)
  //     .map((x) => x.replace("</p>", "").trim());
  // }

  function attemptToTransformBillDescription(description: string | null) {
    if (!description) {
      return <p>No description found.</p>;
    }

    const split = description.trim().split("<p>");

    if (split.length > 1) {
      return (
        <ul>
          {split
            .slice(
              (split[1].includes("<b>") || split[1].includes("<strong>")) &&
                split.length > 2
                ? 2
                : 1
            )
            .map((x) => x.replace("</p>", "").trim())
            .map((x, i) => (
              <li key={i} dangerouslySetInnerHTML={{ __html: x }}></li>
            ))}
        </ul>
      );
    }

    return description.replace(/<\/?[^>]+(>|$)/g, ""); // strip html from string
  }

  // decided to show most recent action for one of the tags (all actions are displayed on the timeline)

  if (bill) {
    return (
      <>
        <div className="block">
          <div className="title">
            {bill.name.type} {bill.name.number}: {bill.name.title}
          </div>

          <p className="subtitle mb-4">
            <span>
              {bill.congress}th Congress, Introduced in the{" "}
              {bill.chamber}
            </span>
          </p>

          <div className="tags">
            {bill.subject_area ? (
              <span className="tag is-info long-tag">{bill.subject_area}</span>
            ) : (
              ""
            )}
            <span className="tag long-tag">{bill.actions[0].action}</span>
          </div>

          {bill.full_text_link ? (
            <a
              href={bill.full_text_link}
              target="_blank"
              className="button is-link mt-0"
              rel="noreferrer"
            >
              Read Full Text
            </a>
          ) : (
            ""
          )}
        </div>

        <div className="block content">
          <div className="title is-size-4 mb-2">Description</div>

          {attemptToTransformBillDescription(bill.description)}
        </div>

        <div className="block">
          <div className="title is-size-4 mb-2">Sponsors</div>
          <div className="tags">
            {bill.relations.sponsors.map((congressperson, i) => (
              <Link to={"/congresspeople/" + congressperson.id} key={i}>
                <span className="tag">{congressperson.name}</span>
              </Link>
            ))}
          </div>
        </div>

        <div className="block">
          <div className="title is-size-4 mb-2">Committees</div>
          <div className="tags">
            {bill.relations.committees.map((committee, i) => (
              <Link to={"/committees/" + committee.id} key={i}>
                <span className="tag">{committee.name}</span>
              </Link>
            ))}
          </div>
        </div>

        <div className="columns">
          {bill.relations.total_votes_nay > 0 &&
          bill.relations.total_votes_yea > 0 ? (
            <div className="column is-two-thirds">
              <div className="block">
                <h2 className="title is-size-4 mb-2">Votes</h2>

                <div className="table-container">
                  <table className="table is-fullwidth">
                    <thead>
                      <tr>
                        <th className="has-background-success-light has-text-success-dark">
                          YEA ({bill.relations.total_votes_yea})
                        </th>
                        <th className="has-background-danger-light has-text-danger-dark">
                          NAY ({bill.relations.total_votes_nay})
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <div className="tags">
                            {bill.relations.voted_yea.map(
                              (congressperson, i) => (
                                <Link
                                  to={"/congresspeople/" + congressperson.id}
                                  key={i}
                                >
                                  <span className="tag">
                                    {congressperson.name}
                                  </span>
                                </Link>
                              )
                            )}
                          </div>
                        </td>
                        <td>
                          <div className="tags">
                            {bill.relations.voted_nay.map(
                              (congressperson, i) => (
                                <Link
                                  to={"/congresspeople/" + congressperson.id}
                                  key={i}
                                >
                                  <span className="tag">
                                    {congressperson.name}
                                  </span>
                                </Link>
                              )
                            )}
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
          <div className="column">
            <div className="block">
              <h2 className="title is-size-4 mb-2">Actions</h2>
              <Timeline
                data={bill.actions.map((x) => ({
                  title: new Date(x.date).toLocaleDateString("en-us", {
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                  }),
                  description: <p>{x.action}</p>,
                }))}
              />
            </div>
          </div>
        </div>
      </>
    );
  } else {
    return <></>;
  }
}
