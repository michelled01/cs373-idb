import { useParams } from "react-router";
import { useGetData } from "../api";
import { Committee } from "../types";
import { Link } from "react-router-dom";

export default function CommitteeView() {
  const { id } = useParams();
  const committee = useGetData<Committee>("/committee/" + id);

  if (committee) {
    return (
      <>
        <div className="block">
          <h1 className="title">
            {committee.name} ({committee.type})
          </h1>
          <h3 className="subtitle mb-2">
            Established{" "}
            {new Date(committee.established_date).toLocaleDateString("en-us", {
              year: "numeric",
              month: "long",
              day: "numeric",
            })}
          </h3>
          <div className="tags mb-2">
            <span
              className={
                committee.active ? "tag is-success" : "tag is-danger"
              }
            >
              {committee.active ? "Active" : "Terminated"}
            </span>
            <span className="tag is-info">{committee.chamber} Committee</span>
          </div>

          <a
            href={committee.website_url}
            target="_blank"
            className="button is-primary mt-0 mb-4"
            rel="noreferrer"
          >
            Open Official Congressional Site
          </a>

          {/* <p>
            <img
              src={"/assets/committee_logos/" + committee.id + ".png"}
              className="committee-logo my-3"
              alt={committee.name}
            />
          </p> */}

          <div className="block">
            <h2 className="title is-size-4 mb-2">Chairperson</h2>
            <div className="tags">
              <Link
                to={"/congresspeople/" + committee.relations.chair.id}
                className="is-underlined"
              >
                <span className="tag">{committee.relations.chair.name}</span>
              </Link>
            </div>
          </div>
        </div>

        <div className="block">
          <h2 className="title is-size-4">Congresspeople</h2>
          <h3 className="subtitle mb-2 is-size-6">
            {committee.relations.total_members} total
          </h3>

          <div className="tags">
            {committee.relations.members.map((congressperson, i) => (
              <Link to={"/congresspeople/" + congressperson.id} key={i}>
                <span className="tag">{congressperson.name}</span>
              </Link>
            ))}
          </div>
        </div>

        <div className="block">
          <h2 className="title is-size-4">Bills</h2>
          <h3 className="subtitle mb-2 is-size-6">
            {committee.relations.total_bills} total
          </h3>

          <div className="tags">
            {committee.relations.bills.map((bill, i) => (
              <Link to={"/bills/" + bill.id} key={i}>
                <span className="tag long-tag">{bill.name}</span>
              </Link>
            ))}
          </div>
        </div>
      </>
    );
  } else {
    return <></>;
  }
}
