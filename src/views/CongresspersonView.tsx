import { useParams } from "react-router";
import { useGetData } from "../api";
import { Congressperson } from "../types";
import { Link } from "react-router-dom";

export default function CongresspersonView() {
  const { id } = useParams();
  const congressperson = useGetData<Congressperson>("/congressperson/" + id);

  if (congressperson) {
    return (
      <>
        <div className="columns">
          {congressperson.image_url ? (
            <div className="column is-narrow">
              <img
                width={200}
                height={240}
                src={congressperson.image_url}
                alt={congressperson.name.first + " " + congressperson.name.last}
              ></img>
            </div>
          ) : (
            ""
          )}

          <div className="column">
            <div className="block">
              <h1 className="title">
                {congressperson.name.first} {congressperson.name.last} (
                {congressperson.party_initial}-
                {congressperson.state_abbreviation})
              </h1>
              <p className="subtitle mb-3">
                {congressperson.chamber}
                {congressperson.district ? (
                  <span>, District {congressperson.district}</span>
                ) : (
                  ""
                )}
              </p>
              {congressperson.website_url ? (
                <a href={congressperson.website_url} className="button is-link mb-1" target="_blank" rel="noreferrer">
                  Open Website
                </a>
              ) : (
                ""
              )}
            </div>

            <div className="block">
              <h2 className="title is-size-4 mb-2">Bills Sponsored</h2>

              <div className="tags">
                {congressperson.relations.sponsored_bills.length > 0 ? congressperson.relations.sponsored_bills.map((bill, i) => (
                  <Link to={"/bills/" + bill.id} key={i}>
                    <span className="tag long-tag">{bill.name}</span>
                  </Link>
                )) : <p>No sponsored bills.</p>}
              </div>
            </div>

            <div className="block">
              <h2 className="title is-size-4 mb-2">Committees</h2>
              <div className="tags are-medium">
                {congressperson.relations.committees.map((committee, i) => (
                  <Link to={"/committees/" + committee.id} key={i}>
                    <span className="tag long-tag">{committee.name}</span>
                  </Link>
                ))}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  } else {
    return <></>;
  }
}
