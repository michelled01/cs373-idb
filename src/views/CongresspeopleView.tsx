import { Search } from "../components/Search";
import { Link } from "react-router-dom";
import { useGetData } from "../api";
import { Paginated, Congressperson } from "../types";
import { Suspense, useState, useEffect } from "react";
import { Pagination } from "react-bulma-components";
import { highlightText } from "./BillsView";

function CongresspeopleTableData(props: {
  setNumPages(numPages: number): void;
  page: number;
  urlParams: any;
  searchTerms: any;
}) {
  const congresspeople = useGetData<Paginated<Congressperson>>(
    `/congresspeople?page=${props.page}&items_per_page=16&${props.urlParams}`
  );

  useEffect(() => {
    if (congresspeople) {
      props.setNumPages(
        Math.ceil(congresspeople.total_items / congresspeople.items_per_page)
      );
    }
  });

  if (congresspeople) {
    return (
      <>
        {congresspeople.items.map((x, i) => (
          <tr key={i}>
            <th>
              <Link to={x.id.toString()} className="is-underlined"
                dangerouslySetInnerHTML={{ __html: highlightText(x.name.first + ' ' + (x.name.middle_initial ? x.name.middle_initial : "") + ' ' + x.name.last, props.searchTerms) }}>
              </Link>
            </th>
            <td>
              <div className="is-flex is-align-items-center">
                <span
                  className={
                    "dot mr-2" +
                    (x.party === "Republican"
                      ? " is-republican"
                      : x.party === "Democrat" || x.party === "Democratic"
                        ? " is-democrat"
                        : "")
                  }
                ></span>
                <span
                  dangerouslySetInnerHTML={{ __html: highlightText(x.party, props.searchTerms) }}>
                </span>
              </div>
            </td>
            <td
              dangerouslySetInnerHTML={{ __html: highlightText(x.state, props.searchTerms) }}>
            </td>
            <td
              dangerouslySetInnerHTML={{ __html: highlightText(x.chamber, props.searchTerms) }}>
            </td>
            <td>
              {/* TODO possibly truncate this to a certain number of lines on smaller devices? */}
              <div className="tags">
                {x.relations.committees.map((committee, i) => (
                  <Link to={"/committees/" + committee.id} key={i}>
                    <span className="tag"
                      dangerouslySetInnerHTML={{ __html: highlightText(committee.name, props.searchTerms) }}>
                    </span>
                  </Link>
                ))}
              </div>
            </td>
          </tr >
        ))
        }
      </>
    );
  } else {
    return <></>;
  }
}

function CongresspeopleSkeleton(props: { count: number }) {
  return (
    <>
      {Array(props.count)
        .fill(null)
        .map((_, i) => (
          <tr key={i} style={{ height: "42px" }}>
            <th>
              <div className="skeleton skeleton-text skeleton-text__body mt-2"></div>
            </th>
            <td>
              <div className="skeleton skeleton-text skeleton-text__body mt-2"></div>
            </td>
            <td>
              <div className="skeleton skeleton-text skeleton-text__body mt-2"></div>
            </td>
            <td>
              <div className="skeleton skeleton-text skeleton-text__body mt-2"></div>
            </td>
            <td>
              <div className="skeleton skeleton-text skeleton-text__body mt-2"></div>
            </td>
          </tr>
        ))}
    </>
  );
}

export default function CongresspeopleView() {
  const [urlParams, updateURLParams] = useState("")
  const [searchTerms, updateSearchTerms] = useState<string[]>([])
  const [page, setPage] = useState(1);
  function setPageAndScroll(page: number) {
    setPage(page)
    window.scrollTo(0, 0)
  }
  const [numPages, setNumPages] = useState(1);

  return (
    <>
      <h1 className="title">Congresspeople</h1>
      <Search updateURLParams={updateURLParams} updateSearchTerms={updateSearchTerms} modelEndpoint='/congressperson_properties' />
      <div className="table-container">
        <table className="table is-fullwidth">
          <thead>
            <tr>
              <th>Name</th>
              <th>Party</th>
              <th>State</th>
              <th>Chamber</th>
              <th>Committees</th>
            </tr>
          </thead>
          <tbody>
            <Suspense fallback={<CongresspeopleSkeleton count={16} />}>
              <CongresspeopleTableData setNumPages={setNumPages} page={page} urlParams={urlParams} searchTerms={searchTerms} />
            </Suspense>
          </tbody>
        </table>
      </div>
      <div className="is-flex-grow-1"></div>
      <Pagination
        current={page}
        onChange={setPageAndScroll}
        showFirstLast
        total={numPages}
        delta={2}
      />
    </>
  );
}
