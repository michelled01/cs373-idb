import React, { Suspense } from "react";
import { ErrorBoundary } from "react-error-boundary";
// import logo from "./logo.svg";
import Navbar from "./components/Navbar";
import { Outlet } from "react-router-dom";
import Footer from "./components/Footer";
import ScrollToTop from "./components/ScrollToTop";

function App() {
  return (
    <div className="App">
      <ScrollToTop/>
      <Navbar />
      {/* TODO remove this min-100-vh when we get more content, its here for padding right now */}
      <section className="section min-height-temp-padding is-flex">
        <div className="container is-flex is-flex-direction-column w-100">
          <ErrorBoundary fallback={<div>There was an error when getting data!</div>}>
            <Suspense fallback="Loading Data...">
              <Outlet />
            </Suspense>
          </ErrorBoundary>
        </div>
      </section>
      <Footer />
    </div>
  );
}

export default App;
