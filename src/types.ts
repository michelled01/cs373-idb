// no need for previews of each of the 3 objects since they share field names and types
export interface Preview {
    "id": number,
    "name": string,
}

export interface Bill {
    "id": number,
    "name": {
        "type": "H.R." | "S.B." | "H.J.R." | "S.J.R." | "H.C.R." | "S.C.R." | "H.S.R." | "S.S.R.",
        "number": number,
        "title": string,
    },
    "description": string | null,
    "congress": number,
    "chamber": "House" | "Senate",
    "full_text_link": string | null,
    "actions": [
        {
            "sequence_number": number,
            "date": string,
            "action": "Introduced in House" | "Introduced in Senate" | "Passed in House" | "Passed in Senate" | "Resolving Differences" | "Presented to President" | "Vetoed by President" | "Pocket vetoed by President" | "Passed over veto" | "Failed to pass over veto" | "Became Law"
        }
    ],
    "subject_area": "Agriculture and Food" | "Animals" | "Armed Forces and National Security" | "Arts, Culture, Religion" | "Civil Rights and Liberties, Minority Issues" | "Commerce" | "Congress" | "Crime and Law Enforcement" | "Economics and Public Finance" | "Education" | "Emergency Management" | "Energy" | "Environmental Protection" | "Families" | "Finance and Financial Sector" | "Foreign Trade and International Finance" | "Government Operations and Politics" | "Health" | "Housing and Community Development" | "Immigration" | "International Affairs" | "Labor and Employment" | "Law" | "Native Americans" | "Public Lands and Natural Resources" | "Science, Technology, Communications" | "Social Sciences and History" | "Social Welfare" | "Sports and Recreation" | "Taxation" | "Transportation and Public Works" | "Water Resources Development" | null,
    "relations": {
        "total_committees": number,
        "total_sponsors": number,
        "total_votes_yea": number,
        "total_votes_nay": number,
        "committees": [Preview],
        "sponsors": [Preview],
        "voted_yea": [Preview],
        "voted_nay": [Preview],
    },
}

export interface Congressperson {
    "id": number,
    "name": {
        "last": string,
        "first": string,
        "middle_initial": string | null,
    },
    "party": string | null,
    "party_initial": string | null,
    "image_url": string | null,
    "website_url": string | null,
    "chamber": "House" | "Senate",
    "district": number | null, // null if senator
    "state": string,
    "state_abbreviation": string,
    "relations": {
        "total_committees": number,
        "total_sponsored_bills": number,
        "total_votes_yea": number,
        "total_votes_nay": number,
        "chairing": [Preview],
        "committees": [Preview],
        "sponsored_bills": [Preview],
        "bills_voted_yea": [Preview],
        "bills_voted_nay": [Preview],
    },
}

export interface Committee {
    "id": number,
    "name": string,
    "type": "Standing" | "Special" | "Select" | "Other" | "Joint" | "Commission or Caucus",
    "chamber": "House" | "Senate" | "Joint",
    "established_date": string,
    "active": boolean,
    "website_url": string,
    "relations": {
        "total_members": number,
        "total_bills": number,
        "chair": Preview,
        "members": [Preview],
        "bills": [Preview],
    },
}

export interface Paginated<T> {
    "page": number,
    "items_per_page": number,
    "total_items": number,
    "items_this_page": number,
    "sort_by": string,
    "sort_ascending": boolean,
    "items": T[],
}

export interface GitlabStats {
    "COUNTS": any,
    "total_commits": number,
    "total_issues": number,
    "total_unit_tests": number,
    "commits": any,
    "issues": any,
    "unit_tests": any,
    "gitlab_repo_url": string,
    "gitlab_wiki_url": string,
    "gitlab_issue_tracker_url": string,
    "postman_url": string,
}