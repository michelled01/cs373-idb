import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
} from "react-router-dom";

import App from "./App";

import BillsView from "./views/BillsView";
import CommitteesView from "./views/CommitteesView";
import CongresspeopleView from "./views/CongresspeopleView";
import AboutView from "./views/AboutView";
import BillView from "./views/BillView";
import HomeView from "./views/HomeView";
import CommitteeView from "./views/CommitteeView";
import CongresspersonView from "./views/CongresspersonView";
import NotFound from "./views/NotFound";

export const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<App />}>
      <Route index element={<HomeView />} />
      <Route path="bills">
        <Route index element={<BillsView />} />
        <Route path=":id" element={<BillView />} />
      </Route>
      <Route path="committees">
        <Route index element={<CommitteesView />} />
        <Route path=":id" element={<CommitteeView />} />
      </Route>
      <Route path="congresspeople">
        <Route index element={<CongresspeopleView />} />
        <Route path=":id" element={<CongresspersonView />} />
      </Route>
      <Route path="about_legislore" element={<AboutView />} />
      <Route path='*' element={<NotFound />}/>
    </Route>
  )
);
