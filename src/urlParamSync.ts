
import { useSearchParams } from "react-router-dom";

export function useSearchParameters() {
    let [searchParams, setSearchParams] = useSearchParams();

    function setSearchParam(key: string, value: any) {
        setSearchParams(params => {
            params.set(key, value);
            return params;
        });
    }

    function getSearchParam(key: string) {
        return searchParams.get(key)
    }

    function getDefaultParams() {
        const url = new URLSearchParams()
        const sortAscending = getSearchParam("sort_ascending")
        const sortBy = getSearchParam('sort_by')
        const filterBy = getSearchParam('filter_by')
        const filterOp = getSearchParam('op')
        const filterValue = getSearchParam('filter_value')
        if (sortAscending != null) url.append("sort_ascending", sortAscending.toString())
        if (sortBy) url.append("sort_by", sortBy)
        if (filterBy && filterOp) url.append(filterBy.toString(), `${filterOp}_${filterValue}`)
        return url.toString()
    }

    return { getSearchParam, setSearchParam, searchParams, setSearchParams, getDefaultParams }
}
