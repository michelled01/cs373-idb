# LegisLore

LegisLore aims to provide at-a-glance insights into the bills in the national congress, the congresspeople who voted {Y/N/other} on them, and the committees that introduced the bills. (The relation between congresspeople and committees is membership.)

We use Typescript, React, and Tailwind CSS. This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started 
Install JS Dependencies (Node JS Required)
```
npm install
```

You might have to install firebase-tools globally:
```
npm install -g firebase-tools
```

Install Python dependencies
- Create a new Python Virtual Environment with `python -m venv .venv`
- Activate the environment: 
    - Go to the VS Code Command Pallete (Ctrl-Shift-P). - Search for and click "Select Interpreter". 
    - Select the interpreter you just created inside the .venv directory. 
    - Create a new terminal, and the environment will be activated. VS Code will now activate the environment in any new terminal.
    - (You can manually activate the environment by running the activate script in .venv/scripts)
- Run `pip install -r requirements.txt` to install dependencies.

## Firebase deployment (Front End)

```
firebase login
```
This command lets you authenticate the Firebase CLI with your Google Account credentials.
You should only have to do this once.

```
npm run build
```
This builds the site locally on your machine and dumps the files into the `build` directory.

```
firebase deploy
```
This actually deploys the site!

```
firebase hosting:channel:deploy dev
```
Or instead deploy the site to a preview branch called dev.


## App Engine Deployment (Backend)

```
glcoud app deploy
```
This command deploys the backend app.

## Data sources:
- https://api.congress.gov/ (primary)
- https://api.govinfo.gov/docs/ (primary)
- https://www.mediawiki.org/wiki/API:Main_page (secondary to fill in any gaps, data perhaps less reliable than official sources. better suited for attributes of our 3 entities than the relations between them)
we may possibly scrape some data from https://clerk.house.gov/ (pages are server-side rendered) if needed, though it seems pretty redundant with the other sources so this is unlikely


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
